/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"

#include "util/string.h"
#include "util/debug.h"

#include "mm/mmobj.h"
#include "mm/pframe.h"
#include "mm/mm.h"
#include "mm/page.h"
#include "mm/slab.h"
#include "mm/tlb.h"

int anon_count = 0; /* for debugging/verification purposes */

static slab_allocator_t *anon_allocator;

static void anon_ref(mmobj_t *o);
static void anon_put(mmobj_t *o);
static int  anon_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf);
static int  anon_fillpage(mmobj_t *o, pframe_t *pf);
static int  anon_dirtypage(mmobj_t *o, pframe_t *pf);
static int  anon_cleanpage(mmobj_t *o, pframe_t *pf);

static mmobj_ops_t anon_mmobj_ops = {
        .ref = anon_ref,
        .put = anon_put,
        .lookuppage = anon_lookuppage,
        .fillpage  = anon_fillpage,
        .dirtypage = anon_dirtypage,
        .cleanpage = anon_cleanpage
};

/*
 * This function is called at boot time to initialize the
 * anonymous page sub system. Currently it only initializes the
 * anon_allocator object.
 */
void
anon_init()
{
        /*NOT_YET_IMPLEMENTED("VM: anon_init");*/
        anon_allocator = slab_allocator_create("anon", sizeof(mmobj_t));
        KASSERT(anon_allocator);
        dbg(DBG_PRINT, "(GRADING3A 4.a)\n");
}

/*
 * You'll want to use the anon_allocator to allocate the mmobj to
 * return, then then initialize it. Take a look in mm/mmobj.h for
 * macros which can be of use here. Make sure your initial
 * reference count is correct.
 */
mmobj_t *
anon_create()
{
        /*NOT_YET_IMPLEMENTED("VM: anon_create");*/
        mmobj_t *anon_obj = (mmobj_t *)slab_obj_alloc(anon_allocator);
       	KASSERT(anon_obj);
        dbg(DBG_PRINT, "(GRADING3A)\n");
        
        anon_count++;
        /*
		 * Since by default the object is initialized as not a shadow object,
		 * we intialize the mmo_vmas member of its union here
		 */
		mmobj_init(anon_obj, &anon_mmobj_ops);
		anon_obj->mmo_refcount =1;
		
		return anon_obj;
}

/* Implementation of mmobj entry points: */

/*
 * Increment the reference count on the object.
 */
static void
anon_ref(mmobj_t *o)
{
        /*NOT_YET_IMPLEMENTED("VM: anon_ref");*/
        KASSERT(o && (0 < o->mmo_refcount) && (&anon_mmobj_ops == o->mmo_ops));
        dbg(DBG_PRINT, "(GRADING3A 4.b)0x%x:ref %d\n",(unsigned int)o,o->mmo_refcount);
        
        o->mmo_refcount++;
}

/*
 * Decrement the reference count on the object. If, however, the
 * reference count on the object reaches the number of resident
 * pages of the object, we can conclude that the object is no
 * longer in use and, since it is an anonymous object, it will
 * never be used again. You should unpin and uncache all of the
 * object's pages and then free the object itself.
 */
static void
anon_put(mmobj_t *o)
{
        /*NOT_YET_IMPLEMENTED("VM: anon_put");*/
        KASSERT(o && (0 < o->mmo_refcount) && (&anon_mmobj_ops == o->mmo_ops));
        dbg(DBG_PRINT, "(GRADING3A 4.c)0x%x:ref %d\n",(unsigned int)o,o->mmo_refcount);
       	
        /*No longer in use, unpin and free all the object`s pages and free mmobj it self*/
        if(o->mmo_nrespages == (o->mmo_refcount-1)) {
        	dbg(DBG_PRINT, "(GRADING3B)\n");
        	int res_pn=0;/*resident page number*/
        	pframe_t *pf = NULL;
        	list_iterate_begin(&o->mmo_respages, pf, pframe_t, pf_olink) {
        		dbg(DBG_PRINT, "(GRADING3B)\n");
        		/*unpin and uncache/free*/
        		/*while(pframe_is_busy(pf)) {
        			dbg(DBG_TEST, "(chaochao)\n");
					sched_sleep_on(&pf->pf_waitq);
				}*/
        		while(pframe_is_pinned(pf)) {
        			dbg(DBG_PRINT, "(GRADING3B)\n");
        			pframe_unpin(pf);
        		}	
				pframe_free(pf);
				page_free(pf->pf_addr);		
        	} list_iterate_end();
        	/*Free the anon_obj itself*/
        	slab_obj_free(anon_allocator, o);
        	anon_count--;
        }
       o->mmo_refcount--;
}

/* Get the corresponding page from the mmobj. No special handling is
 * required. */
static int
anon_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf)
{
        /*NOT_YET_IMPLEMENTED("VM: anon_lookuppage");*/
        
        dbg(DBG_PRINT, "(GRADING3A)\n");	
        return pframe_get(o, pagenum, pf);
	

	
}

/* The following three functions should not be difficult. */

static int
anon_fillpage(mmobj_t *o, pframe_t *pf)
{
        /*NOT_YET_IMPLEMENTED("VM: anon_fillpage");*/
        KASSERT(pframe_is_busy(pf));
        dbg(DBG_PRINT, "(GRADING3A 4.d)\n");
        KASSERT(!pframe_is_pinned(pf));
        dbg(DBG_PRINT, "(GRADING3A 4.d)\n");
        anon_dirtypage(o, pf);
        pframe_set_dirty(pf);
        /*Anon obj should be zeroed out!*/
        memset(pf->pf_addr, '\0', PAGE_SIZE);
       
        return 0;
}

/*Called when a request is made to dirty a non-dirty page.
 * Perform any necessary actions that must take place in order for it
 * to be possible to dirty (write to) the provided page. (For example,
 * if this page corresponds to a sparse block of a file that belongs to
 * an S5 filesystem, it would be necessary/desirable to allocate a
 * block in the fs before allowing a write to the block to proceed).
 * This may block.
 * Return 0 on success and -errno otherwise.
 */
static int
anon_dirtypage(mmobj_t *o, pframe_t *pf)
{
        /*NOT_YET_IMPLEMENTED("VM: anon_dirtypage");*/
        
	/*First make sure it won`t be paged-out*/
	dbg(DBG_PRINT,"(GRADING3A)\n");
	if(!pframe_is_pinned(pf)) {
		dbg(DBG_PRINT, "(GRADING3B)\n");	
		pframe_pin(pf);
	}	
	return 0;
}



/* Write the contents of the page frame starting at address
* pf->pf_addr to the page identified by pf->pf_obj and
* pf->pf_pagenum.
* This may block.
* Return 0 on success and -errno otherwise.*/
static int
anon_cleanpage(mmobj_t *o, pframe_t *pf)
{
    /*NOT_YET_IMPLEMENTED("VM: anon_cleanpage");*/
    /*dbg(DBG_TEST,"(chaochao)\n");
	while(pframe_is_pinned(pf)) {
		dbg(DBG_TEST,"(chaochao)\n");
		pframe_unpin(pf);
    }*/
    return 0;
}


