/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"
#include "types.h"

#include "mm/mm.h"
#include "mm/tlb.h"
#include "mm/mman.h"
#include "mm/page.h"

#include "proc/proc.h"

#include "util/string.h"
#include "util/debug.h"

#include "fs/vnode.h"
#include "fs/vfs.h"
#include "fs/file.h"

#include "vm/vmmap.h"
#include "vm/mmap.h"

/*
 * This function implements the mmap(2) syscall, but only
 * supports the MAP_SHARED, MAP_PRIVATE, MAP_FIXED, and
 * MAP_ANON flags.
 *
 * Add a mapping to the current process's address space.
 * You need to do some error checking; see the ERRORS section
 * of the manpage for the problems you should anticipate.
 * After error checking most of the work of this function is
 * done by vmmap_map(), but remember to clear the TLB.
 */
int
do_mmap(void *addr, size_t len, int prot, int flags,
        int fd, off_t off, void **ret)
{
        /*NOT_YET_IMPLEMENTED("VM: do_mmap");*/
        
        /*err checking actually is almost done in vmmap*/
        /*EACCES EBADF EINVAL*/
	
        vnode_t * vn = NULL;
        file_t *file = NULL;
		
		if ((len <= 0) || (!PAGE_ALIGNED(off)) ||
				(!(flags & MAP_PRIVATE) && !(flags & MAP_SHARED)) || ((flags & MAP_PRIVATE) && (flags & MAP_SHARED))) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
			return -EINVAL;
		}
		

		
		if ((addr != NULL) && (((uint32_t)addr < USER_MEM_LOW) || ((uint32_t)addr >= USER_MEM_HIGH) || (len > USER_MEM_HIGH - USER_MEM_LOW) || (uint32_t)addr + len >= USER_MEM_HIGH)) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
			return -EINVAL;
		}
		
		if (addr == NULL && (flags & MAP_FIXED)) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
			return -EINVAL;	
		}
		
    if(!(flags & MAP_ANON)){
        	dbg(DBG_PRINT, "(GRADING3A)not MAP_ANON\n");
        	KASSERT(fd >= 0 || fd <= NFILES);
        	
        	file = fget(fd);
        	
		    if(file == NULL){
		    	dbg(DBG_PRINT,"(GRADING3D 2)EBADF\n");
		      	return -EBADF;

		    }
		    
		    if ((flags & MAP_PRIVATE) && !(file->f_mode & FMODE_READ)) {
				fput(file);
				dbg(DBG_PRINT,"(GRADING3D 2)\n");
				return -EACCES;
			}
			
			if ((flags & MAP_SHARED) && (prot & PROT_WRITE ) && ((file->f_mode & (FMODE_READ | FMODE_WRITE)) != (FMODE_READ | FMODE_WRITE))) {
				fput(file);
				dbg(DBG_PRINT,"(GRADING3D 2)\n");
				return -EACCES;
			}
		    vn = file->f_vnode;
		}
        vmarea_t *vma = NULL;
        uint32_t npages = len >= PAGE_SIZE ? len/PAGE_SIZE : 1;
        int err = vmmap_map(curproc->p_vmmap, vn, ADDR_TO_PN(addr), npages,
          prot, flags, off, VMMAP_DIR_HILO, &vma);
        
        if(err < 0) {
        	if(!(flags & MAP_ANON) && file != NULL) {
        		fput(file);
        		dbg(DBG_PRINT,"(GRADING3D 2)\n");
        	}
        	dbg(DBG_PRINT,"(GRADING3D 2)\n");
        	return err;
        }
        
        if(ret != NULL) {
					dbg(DBG_PRINT,"(GRADING3A)\n");
        	*ret = PN_TO_ADDR(vma->vma_start);

        }
       
        

        tlb_flush_all();
        
      
        

    if (!(flags & MAP_ANON) && file != NULL) {
        dbg(DBG_PRINT,"(GRADING3A)\n");
		 	 fput(file);
		}
  	KASSERT(NULL != curproc->p_pagedir);
        dbg(DBG_PRINT, "(GRADING3A 2.a)\n");
        return 0;

}



/*
 * This function implements the munmap(2) syscall.
 *
 * As with do_mmap() it should perform the required error checking,
 * before calling upon vmmap_remove() to do most of the work.
 * Remember to clear the TLB.
 */
int
do_munmap(void *addr, size_t len)
{
        /*NOT_YET_IMPLEMENTED("VM: do_munmap");*/
        if ((((uint32_t)addr < USER_MEM_LOW) || ((uint32_t)addr >= USER_MEM_HIGH) || (len > USER_MEM_HIGH - USER_MEM_LOW) || (uint32_t)addr + len > USER_MEM_HIGH ) || (len==0)) {
        	dbg(DBG_PRINT,"(GRADING3D 2)\n");
					return -EINVAL;
				}
				KASSERT(PAGE_ALIGNED(addr));
        int ret=vmmap_remove(curproc->p_vmmap, ADDR_TO_PN(addr), (len >= PAGE_SIZE ? len/PAGE_SIZE : 1));
        KASSERT(ret==0);
        /*
        if(ret < 0) {
        	dbg(DBG_PRINT,"?\n");
        	return ret;
        }
        */
        tlb_flush_all();
        
        KASSERT(NULL != curproc->p_pagedir);
        dbg(DBG_PRINT, "(GRADING3A 2.b)\n");
        return 0;
}


