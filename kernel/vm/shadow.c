/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"

#include "util/string.h"
#include "util/debug.h"

#include "mm/mmobj.h"
#include "mm/pframe.h"
#include "mm/mm.h"
#include "mm/page.h"
#include "mm/slab.h"
#include "mm/tlb.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/shadowd.h"

#define SHADOW_SINGLETON_THRESHOLD 5

int shadow_count = 0; /* for debugging/verification purposes */
#ifdef __SHADOWD__
/*
 * number of shadow objects with a single parent, that is another shadow
 * object in the shadow objects tree(singletons)
 */
static int shadow_singleton_count = 0;
#endif

static slab_allocator_t *shadow_allocator;

static void shadow_ref(mmobj_t *o);
static void shadow_put(mmobj_t *o);
static int  shadow_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf);
static int  shadow_fillpage(mmobj_t *o, pframe_t *pf);
static int  shadow_dirtypage(mmobj_t *o, pframe_t *pf);
static int  shadow_cleanpage(mmobj_t *o, pframe_t *pf);

static mmobj_ops_t shadow_mmobj_ops = {
        .ref = shadow_ref,
        .put = shadow_put,
        .lookuppage = shadow_lookuppage,
        .fillpage  = shadow_fillpage,
        .dirtypage = shadow_dirtypage,
        .cleanpage = shadow_cleanpage
};

/*
 * This function is called at boot time to initialize the
 * shadow page sub system. Currently it only initializes the
 * shadow_allocator object.
 */
void
shadow_init()
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_init");*/
        shadow_allocator = slab_allocator_create("shadow", sizeof(mmobj_t));
        
        KASSERT(shadow_allocator);
        dbg(DBG_PRINT, "(GRADING3A 6.a)\n");
}

/*
 * You'll want to use the shadow_allocator to allocate the mmobj to
 * return, then then initialize it. Take a look in mm/mmobj.h for
 * macros which can be of use here. Make sure your initial
 * reference count is correct.
 */
mmobj_t *
shadow_create()
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_create");*/
        mmobj_t *shadow_obj = (mmobj_t *)slab_obj_alloc(shadow_allocator);

		KASSERT(shadow_obj);       	
		dbg(DBG_PRINT, "(GRADING3A)\n");
		
       	shadow_count++;
        mmobj_init(shadow_obj, &shadow_mmobj_ops);
        
        shadow_obj->mmo_refcount = 1;
        
        return shadow_obj;
}

/* Implementation of mmobj entry points: */

/*
 * Increment the reference count on the object.
 */
static void
shadow_ref(mmobj_t *o)
{
       /* NOT_YET_IMPLEMENTED("VM: shadow_ref"); */
        KASSERT(o && (0 < o->mmo_refcount) && (&shadow_mmobj_ops == o->mmo_ops));
        dbg(DBG_PRINT, "(GRADING3A 6.b)\n");
		o->mmo_refcount++;
}

/*
 * Decrement the reference count on the object. If, however, the
 * reference count on the object reaches the number of resident
 * pages of the object, we can conclude that the object is no
 * longer in use and, since it is a shadow object, it will never
 * be used again. You should unpin and uncache all of the object's
 * pages and then free the object itself.
 */
 static void

 shadow_put(mmobj_t *o)

 {

 	/*NOT_YET_IMPLEMENTED("VM: shadow_put");*/

 	KASSERT(o && (0 < o->mmo_refcount) && (&shadow_mmobj_ops == o->mmo_ops));

 	dbg(DBG_PRINT, "(GRADING3A 6.c)0x%x ref%d-1\n",(unsigned int)o,o->mmo_refcount);

 	

 	if(o->mmo_nrespages == (o->mmo_refcount-1)) {
		dbg(DBG_PRINT, "(GRADING3B)\n");	
 		pframe_t *pf = NULL;

 		list_iterate_begin(&o->mmo_respages, pf, pframe_t, pf_olink) {
			dbg(DBG_PRINT, "(GRADING3B)\n");
 			while(pframe_is_pinned(pf)) {
				dbg(DBG_PRINT, "(GRADING3B)\n");
 				pframe_unpin(pf);
			}
 			/*while(pframe_is_busy(pf)) {
				dbg(DBG_TEST, "(chaochao)\n");	
 				sched_sleep_on(&pf->pf_waitq);
 			}*/
 			pframe_free(pf);		
			page_free(pf->pf_addr);
 		}list_iterate_end();		

 		slab_obj_free(shadow_allocator, o);

 		shadow_count--;

 	}

     o->mmo_refcount--;

 }



/* This function looks up the given page in this shadow object. The
 * forwrite argument is true if the page is being looked up for
 * writing, false if it is being looked up for reading. This function
 * must handle all do-not-copy-on-not-write magic (i.e. when forwrite
 * is false find the first shadow object in the chain which has the
 * given page resident). copy-on-write magic (necessary when forwrite
 * is true) is handled in shadow_fillpage, not here. It is important to
 * use iteration rather than recursion here as a recursive implementation
 * can overflow the kernel stack when looking down a long shadow chain */
static int
shadow_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf)
{
      /*  NOT_YET_IMPLEMENTED("VM: shadow_lookuppage"); */
	pframe_t *result = NULL;
	mmobj_t *shadow = NULL;
	mmobj_t *bottom = NULL;
	int ret=0;
	shadow = o;
	dbg(DBG_PRINT, "(GRADING3A)\n");	
	if(!forwrite){
		dbg(DBG_PRINT, "(GRADING3B)\n");	
		while(shadow->mmo_shadowed != NULL){
			dbg(DBG_PRINT, "(GRADING3B)\n");	
			result = pframe_get_resident(shadow, pagenum);
			if(result){
				dbg(DBG_PRINT, "(GRADING3B)\n");	
				/*while(pframe_is_busy(result)) {
					dbg(DBG_TEST, "(chaochao)\n");	
    				sched_sleep_on(&result->pf_waitq);
				}*/
				*pf= result;
				return 0;	
			}
			shadow = shadow->mmo_shadowed;
			
		}

				return shadow->mmo_ops->lookuppage(shadow,pagenum,forwrite,pf); /*if it is anon obj,then use it own lookupage,otherwise, pframe_get.*/
			
	}
	else{
		dbg(DBG_PRINT, "(GRADING3B)\n");	
		while(shadow->mmo_shadowed != NULL){
			dbg(DBG_PRINT, "(GRADING3B)\n");	
			result = pframe_get_resident(shadow, pagenum);
			if(result){
				dbg(DBG_PRINT, "(GRADING3B)\n");	
				return pframe_get(o, pagenum, pf);
			}
			shadow = shadow->mmo_shadowed;
			
		}
	
			ret = shadow->mmo_ops->lookuppage(shadow,pagenum,forwrite,pf); /*if it is anon obj,then use it own lookupage,otherwise, pframe_get.*/
			if(ret == 0 ){
				dbg(DBG_PRINT, "(GRADING3B)\n");	
			return pframe_get(o, pagenum, pf);
			}
			else{
			dbg(DBG_PRINT, "(GRADING3D)\n");	
				return -EFAULT;
			}
		

	}
    return 0;
}

/* As per the specification in mmobj.h, fill the page frame starting
 * at address pf->pf_addr with the contents of the page identified by
 * pf->pf_obj and pf->pf_pagenum. This function handles all
 * copy-on-write magic (i.e. if there is a shadow object which has
 * data for the pf->pf_pagenum-th page then we should take that data,
 * if no such shadow object exists we need to follow the chain of
 * shadow objects all the way to the bottom object and take the data
 * for the pf->pf_pagenum-th page from the last object in the chain).
 * It is important to use iteration rather than recursion here as a 
 * recursive implementation can overflow the kernel stack when 
 * looking down a long shadow chain */
static int
shadow_fillpage(mmobj_t *o, pframe_t *pf)
{
    /*NOT_YET_IMPLEMENTED("VM: shadow_fillpage");*/
    KASSERT(pframe_is_busy(pf));
    dbg(DBG_PRINT, "(GRADING3A 6.d)\n");
    KASSERT(!pframe_is_pinned(pf));
    dbg(DBG_PRINT, "(GRADING3A 6.d)\n");
	pframe_t *result = NULL;
	int ret=0;
	pframe_t *p_next=NULL;
	result = pframe_get_resident(o, pf->pf_pagenum);
	KASSERT(result != NULL);
	
	shadow_dirtypage(o, pf);
	pframe_set_dirty(pf);
	/*pframe_get(o->mmo_un.mmo_bottom_obj, pf-> pf_pagenum, &p_next); */

	/*if((ret = shadow_lookuppage(o->mmo_shadowed, pf-> pf_pagenum, 1, &p_next)) !=0){
		dbg(DBG_TEST, "(chaochao)\n");
		memset(result->pf_addr,'\0', PAGE_SIZE);
		return 0;
	}*/

	ret = shadow_lookuppage(o->mmo_shadowed, pf-> pf_pagenum, 0, &p_next);
	KASSERT(ret == NULL);
	memcpy(result->pf_addr,p_next->pf_addr, PAGE_SIZE);

    return 0;
}
/* These next two functions are not difficult. */
/*Called when a request is made to dirty a non-dirty page.
 * Perform any necessary actions that must take place in order for it
 * to be possible to dirty (write to) the provided page. (For example,
 * if this page corresponds to a sparse block of a file that belongs to
 * an S5 filesystem, it would be necessary/desirable to allocate a
 * block in the fs before allowing a write to the block to proceed).
 * This may block.
 * Return 0 on success and -errno otherwise.
 */
static int
shadow_dirtypage(mmobj_t *o, pframe_t *pf)
{
	/*NOT_YET_IMPLEMENTED("VM: shadow_dirtypage");*/	
	/*First make sure it won`t be paged-out*/
	dbg(DBG_PRINT, "(GRADING3A)\n");	
	if(!pframe_is_pinned(pf)) {
		dbg(DBG_PRINT, "(GRADING3B)\n");	
		pframe_pin(pf);
	}
	return 0;
}


/* Write the contents of the page frame starting at address
* pf->pf_addr to the page identified by pf->pf_obj and
* pf->pf_pagenum.
* This may block.
* Return 0 on success and -errno otherwise.
*/
static int
shadow_cleanpage(mmobj_t *o, pframe_t *pf)
{
    /*NOT_YET_IMPLEMENTED("VM: shadow_dirtypage");*/
	/*dbg(DBG_PRINT, "(chaochao)\n");
	while(pframe_is_pinned(pf)) {
		dbg(DBG_PRINT, "(chaochao)\n");	
		pframe_unpin(pf);
	}*/
	return 0;
}

