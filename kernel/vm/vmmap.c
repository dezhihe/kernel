/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "errno.h"
#include "globals.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "proc/proc.h"

#include "util/debug.h"
#include "util/list.h"
#include "util/string.h"
#include "util/printf.h"

#include "fs/vnode.h"
#include "fs/file.h"
#include "fs/fcntl.h"
#include "fs/vfs_syscall.h"

#include "mm/slab.h"
#include "mm/page.h"
#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/mmobj.h"

static slab_allocator_t *vmmap_allocator;
static slab_allocator_t *vmarea_allocator;

void
vmmap_init(void)
{
        vmmap_allocator = slab_allocator_create("vmmap", sizeof(vmmap_t));
        KASSERT(NULL != vmmap_allocator && "failed to create vmmap allocator!");
        vmarea_allocator = slab_allocator_create("vmarea", sizeof(vmarea_t));
        KASSERT(NULL != vmarea_allocator && "failed to create vmarea allocator!");
}

vmarea_t *
vmarea_alloc(void)
{
        vmarea_t *newvma = (vmarea_t *) slab_obj_alloc(vmarea_allocator);
        if (newvma) {
                newvma->vma_vmmap = NULL;
        }
        return newvma;
}

void
vmarea_free(vmarea_t *vma)
{
        KASSERT(NULL != vma);
        slab_obj_free(vmarea_allocator, vma);
}
	
/* a debugging routine: dumps the mappings of the given address space. */
size_t
vmmap_mapping_info(const void *vmmap, char *buf, size_t osize)
{
        KASSERT(0 < osize);
        KASSERT(NULL != buf);
        KASSERT(NULL != vmmap);

        vmmap_t *map = (vmmap_t *)vmmap;
        vmarea_t *vma;
        ssize_t size = (ssize_t)osize;

        int len = snprintf(buf, size, "%21s %5s %7s %8s %10s %12s\n",
                           "VADDR RANGE", "PROT", "FLAGS", "MMOBJ", "OFFSET",
                           "VFN RANGE");

        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
                size -= len;
                buf += len;
                if (0 >= size) {
                        goto end;
                }

                len = snprintf(buf, size,
                               "%#.8x-%#.8x  %c%c%c  %7s 0x%p %#.5x %#.5x-%#.5x\n",
                               vma->vma_start << PAGE_SHIFT,
                               vma->vma_end << PAGE_SHIFT,
                               (vma->vma_prot & PROT_READ ? 'r' : '-'),
                               (vma->vma_prot & PROT_WRITE ? 'w' : '-'),
                               (vma->vma_prot & PROT_EXEC ? 'x' : '-'),
                               (vma->vma_flags & MAP_SHARED ? " SHARED" : "PRIVATE"),
                               vma->vma_obj, vma->vma_off, vma->vma_start, vma->vma_end);
        } list_iterate_end();

end:
        if (size <= 0) {
                size = osize;
                buf[osize - 1] = '\0';
        }
        /*
        KASSERT(0 <= size);
        if (0 == size) {
                size++;
                buf--;
                buf[0] = '\0';
        }
        */
        return osize - size;
}

/* Create a new vmmap, which has no vmareas and does
 * not refer to a process. */
vmmap_t *
vmmap_create(void)
{

        /*NOT_YET_IMPLEMENTED("VM: vmmap_create");*/
	dbg(DBG_PRINT, "(GRADING3B 1)\n");
        vmmap_t *new_vmmap = (vmmap_t *)slab_obj_alloc(vmmap_allocator);
        KASSERT(new_vmmap != NULL); /*{
        	dbg(DBG_PRINT, "vmmap129.............................Not enough kernel memory to create a new vmmap!\n");
        	return NULL;
        }*/
        
        memset(new_vmmap, '\0', sizeof(vmmap_t));
        list_init(&new_vmmap->vmm_list);
        new_vmmap->vmm_proc = NULL;
        return new_vmmap;

}

/* Removes all vmareas from the address space and frees the
 * vmmap struct. */
void
vmmap_destroy(vmmap_t *map)
{

       /* NOT_YET_IMPLEMENTED("VM: vmmap_destroy"); */

        KASSERT(NULL != map);
        dbg(DBG_PRINT, "(GRADING3A 3.a)\n");
        
        /*Do we delete vmareas or perform the relating ummap operation
        */
		vmarea_t *vma = NULL;
		list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
			
			vmmap_remove(map,vma->vma_start,vma->vma_end-vma->vma_start);
			
		} list_iterate_end();
       	
        slab_obj_free(vmmap_allocator, map);
}

/* Add a vmarea to an address space. Assumes (i.e. asserts to some extent)
 * the vmarea is valid.  This involves finding where to put it in the list
 * of VM areas, and adding it. Don't forget to set the vma_vmmap for the
 * area. */
void
vmmap_insert(vmmap_t *map, vmarea_t *newvma)
{

        /*NOT_YET_IMPLEMENTED("VM: vmmap_insert");*/
        KASSERT(NULL != map && NULL != newvma);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");        
        KASSERT(NULL == newvma->vma_vmmap);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");        
        KASSERT(newvma->vma_start < newvma->vma_end);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
        KASSERT(ADDR_TO_PN(USER_MEM_LOW) <= newvma->vma_start && ADDR_TO_PN(USER_MEM_HIGH) >= newvma->vma_end);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
     
        /*Is it safe to assume the new vma is not going to overlay with the others*/
        if(!list_empty(&map->vmm_list)) {
		dbg(DBG_PRINT, "(GRADING3B 1)\n");
        	vmarea_t *vma = NULL;
        	int inserted = 0;
        	/*uint32_t prev_end = 0;*/
        	list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
        		
        		if(newvma->vma_end <= vma->vma_start) {
        			/*KASSERT(newvma->vma_start >= prev_end);*/
				dbg(DBG_PRINT, "(GRADING3D 3)\n");
        			list_insert_before(&vma->vma_plink, &newvma->vma_plink);

        			inserted = 1;
        			goto done;
        		}
        		/*prev_end = vma->vma_end;*/
        	} list_iterate_end();
        	/*If it is not inserted, then we need to insert it onto the tail*/
     	done:
        	if(inserted == 0) {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
        		list_insert_tail(&map->vmm_list, &newvma->vma_plink);
        	}
        } else {
		dbg(DBG_PRINT, "(GRADING3B 1)\n");
        	list_insert_tail(&map->vmm_list, &newvma->vma_plink);
        }
        newvma->vma_vmmap = map;

}

/* Find a contiguous range of free virtual pages of length npages in
 * the given address space. Returns starting vfn for the range,
 * without altering the map. Returns -1 if no such range exists.
 *
 * Your algorithm should be first fit. If dir is VMMAP_DIR_HILO, you
 * should find a gap as high in the address space as possible; if dir
 * is VMMAP_DIR_LOHI, the gap should be as low as possible. */
int
vmmap_find_range(vmmap_t *map, uint32_t npages, int dir)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_find_range");*/
        
        KASSERT(NULL != map);
        dbg(DBG_PRINT, "(GRADING3A 3.c)\n");
        
        KASSERT(0 < npages);
        dbg(DBG_PRINT, "(GRADING3A 3.c)\n");
        

        KASSERT(dir == VMMAP_DIR_HILO || dir == VMMAP_DIR_LOHI);
        uint32_t usr_low_pn = ADDR_TO_PN(USER_MEM_LOW);
        uint32_t usr_high_pn = ADDR_TO_PN(USER_MEM_HIGH);
        
        /*As low as possible*/
       /* if(dir == VMMAP_DIR_LOHI) {
		dbg(DBG_PRINT, "vmmap237.............................\n");
        	vmarea_t *vma = NULL;
        	uint32_t gap_start = usr_low_pn;
        	uint32_t gap_end = 0;
        	if(!list_empty(&map->vmm_list)) {
			dbg(DBG_PRINT, "vmmap242.............................\n");
        		list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
		    		gap_end = vma->vma_start;
		    		if(gap_end - gap_start >= npages) {
		    			return gap_start;
		    		}
		    		gap_start = vma->vma_end;
		    	} list_iterate_end();			
			if(usr_high_pn - gap_start >= npages){
				dbg(DBG_PRINT, "vmmap251.............................\n");
				return gap_start;
			}

        	}
		else {
        			dbg(DBG_PRINT, "vmmap255.............................\n");
				return usr_low_pn;
        	}
        	
        }*/
        /*As high as possible*/
        if(dir == VMMAP_DIR_HILO) {
		dbg(DBG_PRINT, "(GRADING3B 2)\n");
        	vmarea_t *vma = NULL;
        	uint32_t gap_end = usr_high_pn;
        	uint32_t gap_start = 0;
        	if(!list_empty(&map->vmm_list)) {
			dbg(DBG_PRINT, "(GRADING3B 2)\n");
        		list_iterate_reverse(&map->vmm_list, vma, vmarea_t, vma_plink) {
		    		gap_start = vma->vma_end;
		    		if(gap_end - gap_start >= npages) {
		    			return (gap_end - npages);
		    		}
		    		gap_end = vma->vma_start;
        		} list_iterate_end();
			if(gap_end - usr_low_pn >= npages){
				dbg(DBG_PRINT, "(GRADING3D 3)\n");
				return gap_end - npages;
			}

        	} /*else {
			dbg(DBG_PRINT, "vmmap280.............................\n");
        		return usr_high_pn - npages;
        	}*/
        	
        }
        
        
        return -1;
}

/* Find the vm_area that vfn lies in. Simply scan the address space
 * looking for a vma whose range covers vfn. If the page is unmapped,
 * return NULL. */
vmarea_t *
vmmap_lookup(vmmap_t *map, uint32_t vfn)
{

       /* NOT_YET_IMPLEMENTED("VM: vmmap_lookup");*/
        
        KASSERT(NULL != map);
        dbg(DBG_PRINT, "(GRADING3A 3.d)%x\n",vfn);

        vmarea_t *vma = NULL;
		if(!list_empty(&(map->vmm_list))){

			dbg(DBG_PRINT, "(GRADING3B 1)\n");

			list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
				if(vma->vma_start <= vfn && vfn < vma->vma_end){
  dbg(DBG_PRINT, "(GRADING3A 3.d)%x\n",vfn);
					return vma;
				}
			}list_iterate_end();
		}
		return NULL;
}

/* Allocates a new vmmap containing a new vmarea for each area in the
 * given map. The areas should have no mmobjs set yet. Returns pointer
 * to the new vmmap on success, NULL on failure. This function is
 * called when implementing fork(2). */
vmmap_t *
vmmap_clone(vmmap_t *map)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_clone"); */
	/*dbg(DBG_PRINT, "(GRADING3B 1)\n");*/

	vmmap_t *newmap = NULL;
	newmap =  vmmap_create();
	
	vmarea_t *vma = NULL; 
	if(newmap){
		dbg(DBG_PRINT, "(GRADING3B 1)\n");
	if(!list_empty(&(map->vmm_list))){
		dbg(DBG_PRINT, "(GRADING3B 1)\n");
		list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
			vmarea_t *newvma = NULL; 
			newvma = vmarea_alloc();
			
			KASSERT(newvma != NULL);
			
			newvma->vma_start = vma->vma_start;
			newvma->vma_end = vma->vma_end;
			newvma->vma_off = vma->vma_off;
			newvma->vma_prot =vma->vma_prot;
			newvma->vma_flags = vma->vma_flags;

			vmmap_insert(newmap,newvma);	
		}list_iterate_end();

		}
	}
	return newmap;
}

/* Insert a mapping into the map starting at lopage for npages pages.
 * If lopage is zero, we will find a range of virtual addresses in the
 * process that is big enough, by using vmmap_find_range with the same
 * dir argument.  If lopage is non-zero and the specified region
 * contains another mapping that mapping should be unmapped.
 *
 * If file is NULL an anon mmobj will be used to create a mapping
 * of 0's.  If file is non-null that vnode's file will be mapped in
 * for the given range.  Use the vnode's mmap operation to get the
 * mmobj for the file; do not assume it is file->vn_obj. Make sure all
 * of the area's fields except for vma_obj have been set before
 * calling mmap.
 *
 * If MAP_PRIVATE is specified set up a shadow object for the mmobj.
 *
 * All of the input to this function should be valid (KASSERT!).
 * See mmap(2) for for description of legal input.
 * Note that off should be page aligned.
 *
 * Be very careful about the order operations are performed in here. Some
 * operation are impossible to undo and should be saved until there
 * is no chance of failure.
 *
 * If 'new' is non-NULL a pointer to the new vmarea_t should be stored in it.
 */
int
vmmap_map(vmmap_t *map, vnode_t *file, uint32_t lopage, uint32_t npages,
          int prot, int flags, off_t off, int dir, vmarea_t **new)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_map");*/
        
        KASSERT(NULL != map);
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        
        KASSERT(0 < npages);
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        
        KASSERT(!(~(PROT_NONE | PROT_READ | PROT_WRITE | PROT_EXEC) & prot));
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        
        KASSERT((MAP_SHARED & flags) || (MAP_PRIVATE & flags));
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        
        KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_LOW) <= lopage));
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        
        KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_HIGH) >= (lopage + npages)));
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        
       	KASSERT(PAGE_ALIGNED(off));
        dbg(DBG_PRINT, "(GRADING3A 3.f)\n");
        

		vmarea_t *newvma = NULL;
		newvma = vmarea_alloc();
		if(newvma == NULL) {
			dbg(DBG_PRINT, "(GRADING3D 3)Can not alloc vmarea!\n");
			return -ENOMEM;
		}
		int ret=0;
		if(lopage == 0){
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			int startvpn = vmmap_find_range(map, npages,dir);
			if(startvpn < 0) {
				vmarea_free(newvma);
				dbg(DBG_PRINT, "(GRADING3D 2)Error in vmmap_find_range()!\n");
				return startvpn;
			}
			newvma->vma_start = startvpn;
			newvma->vma_end = startvpn + npages;
		}
		else{
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			newvma->vma_start = lopage;
			newvma->vma_end = lopage + npages;
			if(!vmmap_is_range_empty(map, lopage,npages)){
				dbg(DBG_PRINT, "(GRADING3B 1)\n");
				
				/*In case of removal error*/
				KASSERT((ret = vmmap_remove(map, lopage, npages)) >= 0); /*{
	    			
	    			dbg(DBG_PRINT, "vmmap431.............................Error in vmmap_remove()!\n");
	    			vmarea_free(newvma);
	    			return ret;
	    		}*/
			}
		}
		newvma->vma_off = off / PAGE_SIZE;
		newvma->vma_prot = prot;
		newvma->vma_flags = flags;	
		
		mmobj_t *mmo =NULL;
		if (file) {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			int ret = 0;
			if ((ret = file->vn_ops->mmap(file, newvma, &mmo)) < 0) {
				dbg(DBG_PRINT, "(GRADING3D 3)\n");
				vmarea_free(newvma);
				return ret;
			}
		} else {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			mmo = anon_create();
			KASSERT(mmo != NULL);

		}
		if (flags & MAP_PRIVATE) {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			mmobj_t *shadow =NULL;
			if((shadow = shadow_create())!=NULL){
				dbg(DBG_PRINT, "(GRADING3B 1)\n");
				shadow->mmo_shadowed = mmo;
				shadow-> mmo_un.mmo_bottom_obj = mmo;
				newvma->vma_obj = shadow;
			}
			/*else{
				dbg(DBG_PRINT, "vmmap466.............................\n");
				return -ENOMEM; 				
			}*/
		} else {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
			newvma->vma_obj = mmo;
			
		}
		
        	
        
        /*Do insert late in case of error happening in the middle of the code above*/
		vmmap_insert(map,newvma);
		
		/*list_insert_tail(&mmo->mmo_un.mmo_vmas,&newvma->vma_olink);*/
		list_insert_tail(mmobj_bottom_vmas(newvma->vma_obj),&newvma->vma_olink);
		if(new!=NULL){
				dbg(DBG_PRINT, "(GRADING3B 1)\n");
				*new=newvma;
		}
		
        return 0;
}

/*
 * We have no guarantee that the region of the address space being
 * unmapped will play nicely with our list of vmareas.
 *
 * You must iterate over each vmarea that is partially or wholly covered
 * by the address range [addr ... addr+len). The vm-area will fall into one
 * of four cases, as illustrated below:
 *
 * key:
 *          [             ]   Existing VM Area
 *        *******             Region to be unmapped
 *
 * Case 1:  [   ******    ]
 * The region to be unmapped lies completely inside the vmarea. We need to
 * split the old vmarea into two vmareas. be sure to increment the
 * reference count to the file associated with the vmarea.
 *
 * Case 2:  [      *******]**
 * The region overlaps the end of the vmarea. Just shorten the length of
 * the mapping.
 *
 * Case 3: *[*****        ]
 * The region overlaps the beginning of the vmarea. Move the beginning of
 * the mapping (remember to update vma_off), and shorten its length.
 *
 * Case 4: *[*************]**
 * The region completely contains the vmarea. Remove the vmarea from the
 * list.
 */
int
vmmap_remove(vmmap_t *map, uint32_t lopage, uint32_t npages)
{

        /*NOT_YET_IMPLEMENTED("VM: vmmap_remove");*/
        dbg(DBG_PRINT, "(GRADING3B 1)\n");
        vmarea_t *vma = NULL;
        uint32_t hipage = lopage + npages;/*Exclusive!*/
        
        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
        	/*case 1*/
        	if(vma->vma_start < lopage && vma->vma_end > hipage) {
        		/*vmareas*/
        		dbg(DBG_PRINT, "(GRADING3D 3)\n");
        		
        		vmarea_t *newvma = (vmarea_t *)vmarea_alloc();
        		KASSERT(newvma != NULL);
        		
        		newvma->vma_start = hipage;
        		newvma->vma_end = vma->vma_end;
        		newvma->vma_off = vma->vma_off + hipage-vma->vma_start;
        		newvma->vma_prot = vma->vma_prot;
        		newvma->vma_flags = vma->vma_flags;
        		newvma->vma_vmmap = NULL;
				newvma->vma_obj = vma->vma_obj;
			
				/*Increate refcount of this obj*/
				 newvma->vma_obj = vma->vma_obj;

 				/*Increate refcount of this obj*/

 				newvma->vma_obj->mmo_ops->ref(newvma->vma_obj);

 				/*Also increse the shadow ref!!*/

 				mmobj_t *shadow = vma->vma_obj->mmo_shadowed;

 				/*Should have clone the shadow obj as well*/

 				while(shadow != NULL) {
					dbg(DBG_PRINT, "(GRADING3D 3)\n");

 					(shadow->mmo_ops->ref)(shadow);

 					shadow = shadow->mmo_shadowed;

 				}
				vmmap_insert(map, newvma);
        		vma->vma_end = lopage;
        		/*pframes*/
			list_insert_tail(mmobj_bottom_vmas(newvma->vma_obj), &newvma->vma_olink);
						pt_unmap_range(curproc->p_pagedir, (uintptr_t)PN_TO_ADDR(lopage), (uintptr_t)PN_TO_ADDR(hipage));
        	}
        	/*case 2*/
     		else if(vma->vma_start < lopage && lopage < vma->vma_end && vma->vma_end <= hipage) {
			dbg(DBG_PRINT, "(GRADING3D 3)\n");
        		pt_unmap_range(curproc->p_pagedir, (uintptr_t)PN_TO_ADDR(lopage), (uintptr_t)PN_TO_ADDR(vma->vma_end));
        		vma->vma_end = lopage;

        	}
        	/*case 3*/
        	else if(vma->vma_start >= lopage && hipage > vma->vma_start && vma->vma_end > hipage) {
			dbg(DBG_PRINT, "(GRADING3D 3)\n");
        		pt_unmap_range(pt_get(), (uintptr_t)PN_TO_ADDR(vma->vma_start), (uintptr_t)PN_TO_ADDR(hipage));
			vma->vma_off = vma->vma_off + hipage-vma->vma_start;
        		vma->vma_start = hipage;
        		
        	}
        	/*case 4*/
        	else if(lopage <= vma->vma_start && hipage >= vma->vma_end) {
			dbg(DBG_PRINT, "(GRADING3D 3)\n");
        		pt_unmap_range(curproc->p_pagedir, (uintptr_t)PN_TO_ADDR(vma->vma_start), (uintptr_t)PN_TO_ADDR(vma->vma_end));
        		list_remove(&vma->vma_plink);

			if(list_link_is_linked(&vma->vma_olink)){
				dbg(DBG_PRINT, "(GRADING3D 3)\n");
        			list_remove(&vma->vma_olink);
			}

				int pagenum = 0;
				int total =vma->vma_end-vma->vma_start;
				/*vma->vma_obj->mmo_ops->put(vma->vma_obj);*/
				/*We need to put away all shadows of it*/

 				mmobj_t *mmo = vma->vma_obj;

				mmobj_t *shadow = NULL;

				while(mmo != NULL) {
					dbg(DBG_PRINT, "(GRADING3D 3)\n");
					shadow = mmo->mmo_shadowed;
				 
 					mmo->mmo_ops->put(mmo);

					mmo = shadow;

				}



				/*mmo->mmo_ops->put(mmo);*/
				
				
				for(pagenum = 0;pagenum < total;pagenum++){
					dbg(DBG_PRINT, "(GRADING3D 3)\n");
					pframe_t * pf =NULL;
					if((pf=pframe_get_resident(vma->vma_obj,pagenum)) != NULL && pframe_is_pinned(pf)){
							dbg(DBG_PRINT, "(GRADING3D 3)\n");
							pframe_unpin(pf);					
						}
			}
        		vmarea_free(vma);
        	}
        } list_iterate_end();
        return 0;
}

/*
 * Returns 1 if the given address space has no mappings for the
 * given range, 0 otherwise.
 */
int
vmmap_is_range_empty(vmmap_t *map, uint32_t startvfn, uint32_t npages)
{

       /* NOT_YET_IMPLEMENTED("VM: vmmap_is_range_empty");*/
        
	
        uint32_t endvfn = startvfn + npages;
        KASSERT((startvfn < endvfn) && (ADDR_TO_PN(USER_MEM_LOW) <= startvfn) && (ADDR_TO_PN(USER_MEM_HIGH) >= endvfn));
        dbg(DBG_PRINT, "(GRADING3A 3.e)\n");
		
		vmarea_t *vma = NULL;
        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {

			if(endvfn > vma->vma_start && startvfn < vma->vma_end){
				dbg(DBG_PRINT, "(GRADING3B 1)\n");
				return 0;
			}
		} list_iterate_end();

        
        return 1;
}

/* Read into 'buf' from the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do so, you will want to find the vmareas
 * to read from, then find the pframes within those vmareas corresponding
 * to the virtual addresses you want to read, and then read from the
 * physical memory that pframe points to. You should not check permissions
 * of the areas. Assume (KASSERT) that all the areas you are accessing exist.
 * Returns 0 on success, -errno on error.
 */
int
vmmap_read(vmmap_t *map, const void *vaddr, void *buf, size_t count)
{

        /*NOT_YET_IMPLEMENTED("VM: vmmap_read");*/
	/*Find the correct vmarea to read the buffer from*/
		dbg(DBG_PRINT, "(GRADING3B 1)read cnt:%d\n",count);
		uint32_t startvpn = ADDR_TO_PN(vaddr);
		uint32_t endvpn = ADDR_TO_PN(USER_MEM_HIGH);
	

		/*if(PAGE_ALIGNED((uint32_t)vaddr + count)) {
			dbg(DBG_PRINT, "vmmap679.............................\n");

			endvpn = ADDR_TO_PN((uint32_t)vaddr + count);
		} else {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");*/
			endvpn = ADDR_TO_PN((uint32_t)vaddr + count) + 1;
		/*}*/
		
        /*Go thru the list of */
	void *tmp =NULL;
	tmp = buf;
        KASSERT((vmmap_is_range_empty(map,startvpn,endvpn-startvpn))==0);
        vmarea_t *dest_vma = NULL;
        dest_vma = vmmap_lookup(map, startvpn);
        
        KASSERT(NULL != dest_vma);
       	
       	mmobj_t *dest_obj = dest_vma->vma_obj;
       	uint32_t pn = 0;
       	uint32_t vma_off = dest_vma->vma_off;
       	uint32_t startpage = vma_off + startvpn - dest_vma->vma_start;
       	uint32_t endpage = vma_off + endvpn - dest_vma->vma_start;
       	size_t remain = count;/*Remaining bytes to be read from the pf->pf_addr*/
       	for(pn = startpage; pn < endpage; pn++) {
		dbg(DBG_PRINT, "(GRADING3D 2)\n");
       		pframe_t *pf = NULL;
       		int ret = 0;
		if(dest_vma->vma_flags & MAP_PRIVATE){
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
			KASSERT(dest_obj->mmo_ops->lookuppage(dest_vma->vma_obj, pn, 0, &pf)==0);

		}
		else if((ret = pframe_get(dest_obj, pn,&pf)) < 0) {
				dbg(DBG_PRINT, "(GRADING3D 2)\n");
       				/*Fail to lookup the page*/
       				return ret;
       			}
		
       			/*dirty?*/
       		if(pn == startpage) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
       			uint32_t offset_start = (uint32_t)PAGE_OFFSET(vaddr);
       			
       			memcpy(tmp, (void *)((uint32_t)pf->pf_addr+offset_start), MIN(PAGE_SIZE - offset_start, remain));
       			tmp = (void *)((uint32_t)tmp + MIN(PAGE_SIZE - offset_start, remain));
			remain -= MIN(PAGE_SIZE - offset_start, remain);
       			
       		} else if(pn == endpage - 1) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
       			uint32_t offset_end = (uint32_t)PAGE_OFFSET((uint32_t)vaddr + count);
       			memcpy(tmp, pf->pf_addr, PAGE_SIZE - offset_end);
       			tmp = (void *)((uint32_t)tmp + (PAGE_SIZE - offset_end));
			remain -= (PAGE_SIZE - offset_end);
       		} else {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
       			memcpy(tmp, pf->pf_addr, PAGE_SIZE);
       			tmp = (void *)((uint32_t)tmp + PAGE_SIZE);
			remain -= PAGE_SIZE;
       		}
       		
       	}
        return 0;
}

/* Write from 'buf' into the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do this, you will need to find the correct
 * vmareas to write into, then find the correct pframes within those vmareas,
 * and finally write into the physical addresses that those pframes correspond
 * to. You should not check permissions of the areas you use. Assume (KASSERT)
 * that all the areas you are accessing exist. Remember to dirty pages!
 * Returns 0 on success, -errno on error.
 */
int
vmmap_write(vmmap_t *map, void *vaddr, const void *buf, size_t count)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_write");*/
		/*Find the correct vmarea to write the buffer into*/
		dbg(DBG_PRINT, "(GRADING3B 1)vmmap_write()\n");
		uint32_t startvpn = ADDR_TO_PN(vaddr);
		uint32_t endvpn = ADDR_TO_PN(USER_MEM_HIGH);
		
		if(PAGE_ALIGNED((uint32_t)vaddr + count)) {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			endvpn = ADDR_TO_PN((uint32_t)vaddr + count);
		} else {
			dbg(DBG_PRINT, "(GRADING3B 1)\n");
			endvpn = ADDR_TO_PN((uint32_t)vaddr + count) + 1;
		}
		
        /*Go thru the list of */
        KASSERT((vmmap_is_range_empty(map,startvpn,endvpn-startvpn))==0);
        vmarea_t *dest_vma = NULL;
        dest_vma = vmmap_lookup(map, startvpn);
       	mmobj_t *shadow = NULL;
        KASSERT(NULL != dest_vma);
       
       	mmobj_t *dest_obj = dest_vma->vma_obj;
       	uint32_t pn = 0;
       	uint32_t vma_off = dest_vma->vma_off;
       	uint32_t startpage = vma_off + startvpn - dest_vma->vma_start;
       	uint32_t endpage = vma_off + endvpn - dest_vma->vma_start;
       	size_t remain = count;/*Remaining bytes to be written into the pf->pf_addr*/

       	for(pn = startpage; pn < endpage; pn++) {
		dbg(DBG_PRINT, "(GRADING3D 2)\n");
       		pframe_t *pf = NULL;
       		int ret = 0;


		/* if(dest_vma-> vma_flags & MAP_PRIVATE){

			if((shadow = shadow_create())!=NULL){
 			shadow->mmo_shadowed = dest_vma->vma_obj;
			(dest_vma->vma_obj-> mmo_ops->ref)(dest_vma->vma_obj);

 			shadow-> mmo_un.mmo_bottom_obj = dest_vma->vma_obj->mmo_un.mmo_bottom_obj; 				
			(dest_vma->vma_obj->mmo_un.mmo_bottom_obj-> mmo_ops->ref)(dest_vma->vma_obj->mmo_un.mmo_bottom_obj);
			
			(dest_vma->vma_obj-> mmo_ops->put)(dest_vma->vma_obj);
 			dest_vma->vma_obj = shadow;			
			(shadow-> mmo_ops->ref)(shadow);
			}
			else{
				return -1;
			
			}
			pframe_get(shadow, pn, &pf);
		}

		else{*/ 

 	      	KASSERT((ret = dest_obj->mmo_ops->lookuppage(dest_obj, pn, 1, &pf)) == 0);
       		KASSERT((ret = pframe_dirty(pf))==0);
       		
       		if(pn == startpage) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
       			uint32_t offset_start = (uint32_t)PAGE_OFFSET(vaddr);
       			
       			memcpy((void *)((uint32_t)pf->pf_addr+offset_start), buf, MIN(PAGE_SIZE - offset_start, remain));
       			buf = (void *)((uint32_t)buf + MIN(PAGE_SIZE - offset_start, remain));
			remain -= MIN(PAGE_SIZE - offset_start, remain);
       			
       		} else if(pn == endpage - 1) {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
       			uint32_t offset_end = (uint32_t)PAGE_OFFSET((uint32_t)vaddr + count);
       			memcpy(pf->pf_addr, buf, PAGE_SIZE - offset_end);
       			buf = (void *)((uint32_t)buf + (PAGE_SIZE - offset_end));
			remain -= (PAGE_SIZE - offset_end);
       		} /*else {
			dbg(DBG_PRINT, "(GRADING3D 2)\n");
       			memcpy(pf->pf_addr, buf, PAGE_SIZE);
       			buf = (void *)((uint32_t)buf + PAGE_SIZE);
			remain -= PAGE_SIZE;
       		}*/
       		
       	}
        return 0;
        
        
}



