/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "kernel.h"

#include "util/gdb.h"
#include "util/init.h"
#include "util/debug.h"
#include "util/string.h"
#include "util/printf.h"

#include "mm/mm.h"
#include "mm/page.h"
#include "mm/pagetable.h"
#include "mm/pframe.h"

#include "vm/vmmap.h"
#include "vm/shadowd.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "main/acpi.h"
#include "main/apic.h"
#include "main/interrupt.h"
#include "main/gdt.h"

#include "proc/sched.h"
#include "proc/proc.h"
#include "proc/kthread.h"

#include "drivers/dev.h"
#include "drivers/blockdev.h"
#include "drivers/disk/ata.h"
#include "drivers/tty/virtterm.h"
#include "drivers/pci.h"

#include "api/exec.h"
#include "api/syscall.h"

#include "fs/vfs.h"
#include "fs/vnode.h"
#include "fs/vfs_syscall.h"
#include "fs/fcntl.h"
#include "fs/stat.h"

#include "test/kshell/kshell.h"
#include "errno.h"

GDB_DEFINE_HOOK(boot)
GDB_DEFINE_HOOK(initialized)
GDB_DEFINE_HOOK(shutdown)

static void       hard_shutdown(void);
static void      *bootstrap(int arg1, void *arg2);
static void      *idleproc_run(int arg1, void *arg2);
static kthread_t *initproc_create(void);
static void      *initproc_run(int arg1, void *arg2);

static context_t bootstrap_context;
static int gdb_wait = GDBWAIT;

extern void *sunghan_test(int, void*);
extern void *sunghan_deadlock_test(int, void*);
extern void *faber_thread_test(int, void*);

extern void *vfstest_main(int, void*);
extern int faber_fs_thread_test(kshell_t *ksh, int argc, char **argv);
extern int faber_directory_test(kshell_t *ksh, int argc, char **argv);

/**
 * This is the first real C function ever called. It performs a lot of
 * hardware-specific initialization, then creates a pseudo-context to
 * execute the bootstrap function in.
 */
void
kmain()
{
        GDB_CALL_HOOK(boot);

        dbg_init();
        dbgq(DBG_CORE, "Kernel binary:\n");
        dbgq(DBG_CORE, "  text: 0x%p-0x%p\n", &kernel_start_text, &kernel_end_text);
        dbgq(DBG_CORE, "  data: 0x%p-0x%p\n", &kernel_start_data, &kernel_end_data);
        dbgq(DBG_CORE, "  bss:  0x%p-0x%p\n", &kernel_start_bss, &kernel_end_bss);

        page_init();

        pt_init();
        slab_init();
        pframe_init();

        acpi_init();
        apic_init();
	      pci_init();
        intr_init();

        gdt_init();

        /* initialize slab allocators */
#ifdef __VM__
        anon_init();
        shadow_init();
#endif
        vmmap_init();
        proc_init();
        kthread_init();

#ifdef __DRIVERS__
        bytedev_init();
        blockdev_init();
#endif

        void *bstack = page_alloc();
        pagedir_t *bpdir = pt_get();
        KASSERT(NULL != bstack && "Ran out of memory while booting.");
        /* This little loop gives gdb a place to synch up with weenix.  In the
         * past the weenix command started qemu was started with -S which
         * allowed gdb to connect and start before the boot loader ran, but
         * since then a bug has appeared where breakpoints fail if gdb connects
         * before the boot loader runs.  See
         *
         * https://bugs.launchpad.net/qemu/+bug/526653
         *
         * This loop (along with an additional command in init.gdb setting
         * gdb_wait to 0) sticks weenix at a known place so gdb can join a
         * running weenix, set gdb_wait to zero  and catch the breakpoint in
         * bootstrap below.  See Config.mk for how to set GDBWAIT correctly.
         *
         * DANGER: if GDBWAIT != 0, and gdb is not running, this loop will never
         * exit and weenix will not run.  Make SURE the GDBWAIT is set the way
         * you expect.
         */
        while (gdb_wait) ;
        context_setup(&bootstrap_context, bootstrap, 0, NULL, bstack, PAGE_SIZE, bpdir);
        context_make_active(&bootstrap_context);

        panic("\nReturned to kmain()!!!\n");
}

/**
 * Clears all interrupts and halts, meaning that we will never run
 * again.
 */
static void
hard_shutdown()
{
#ifdef __DRIVERS__
        vt_print_shutdown();
#endif
        __asm__ volatile("cli; hlt");
}

/**
 * This function is called from kmain, however it is not running in a
 * thread context yet. It should create the idle process which will
 * start executing idleproc_run() in a real thread context.  To start
 * executing in the new process's context call context_make_active(),
 * passing in the appropriate context. This function should _NOT_
 * return.
 *
 * Note: Don't forget to set curproc and curthr appropriately.
 *
 * @param arg1 the first argument (unused)
 * @param arg2 the second argument (unused)
 */
static void *
bootstrap(int arg1, void *arg2)
{
        /* necessary to finalize page table information */
        pt_template_init();
	char idle[4] = "idle";
	proc_t * idle_proc=NULL;
	kthread_t * kthread=NULL;
	
	idle_proc = proc_create(idle);
	kthread=kthread_create(idle_proc, idleproc_run, arg1, arg2);
	
	curthr=kthread;
	curproc=idle_proc;
	
	KASSERT(NULL != curproc); /* make sure that the "idle" process has been created successfully */
	dbg(DBG_PRINT, "(GRADING1A 1.a)\n");
	KASSERT(PID_IDLE == curproc->p_pid); /* make sure that what has been created is the "idle" process */
	dbg(DBG_PRINT, "(GRADING1A 1.a)\n");
	KASSERT(NULL != curthr); /* make sure that the thread for the "idle" process has been created successfully */
	dbg(DBG_PRINT, "(GRADING1A 1.a)\n");
	
	
	context_make_active(&(kthread->kt_ctx));
	
        panic("weenix returned to bootstrap()!!! BAD!!!\n");
        return NULL;	
}

/**
 * Once we're inside of idleproc_run(), we are executing in the context of the
 * first process-- a real context, so we can finally begin running
 * meaningful code.
 *
 * This is the body of process 0. It should initialize all that we didn't
 * already initialize in kmain(), launch the init process (initproc_run),
 * wait for the init process to exit, then halt the machine.
 *
 * @param arg1 the first argument (unused)
 * @param arg2 the second argument (unused)
 */
static void *
idleproc_run(int arg1, void *arg2)
{
        int status;
        pid_t child;
	
        /* create init proc */
        kthread_t *initthr = initproc_create();
        init_call_all();
        GDB_CALL_HOOK(initialized);

        /* Create other kernel threads (in order) */

#ifdef __VFS__
        /* Once you have VFS remember to set the current working directory
         * of the idle and init process */
        /*NOT_YET_IMPLEMENTED("VFS: idleproc_run");*/
        dbg(DBG_PRINT, "(GRADING2A)\n");
        curproc->p_cwd = vfs_root_vn;
        vref(curproc->p_cwd);
        if(initthr!=NULL)
        {
	dbg(DBG_PRINT, "(GRADING2A)\n");
        initthr->kt_proc->p_cwd=vfs_root_vn;
        vref(vfs_root_vn);
        }
        

        /* Here you need to make the null, zero, and tty devices using mknod */
        /* You can't do this until you have VFS, check the include/drivers/dev.h
         * file for macros with the device ID's you will need to pass to mknod */
        /*NOT_YET_IMPLEMENTED("VFS: idleproc_run");*/
        KASSERT(do_mkdir("/dev")==0);/*need this? character device?*/
       	KASSERT(do_mknod("/dev/null/",S_IFCHR,MEM_NULL_DEVID)==0);
        KASSERT(do_mknod("/dev/zero",S_IFCHR,MEM_ZERO_DEVID)==0);
        KASSERT(do_mknod("/dev/tty0",S_IFCHR,MKDEVID(2, 0))==0);/*TTY_MAJOR in tty.h*/
#endif

        /* Finally, enable interrupts (we want to make sure interrupts
         * are enabled AFTER all drivers are initialized) */
        intr_enable();

        /* Run initproc */
	
        sched_make_runnable(initthr);
        /* Now wait for it */
        child = do_waitpid(-1, 0, &status);
        KASSERT(PID_INIT == child);

#ifdef __MTP__
        kthread_reapd_shutdown();
#endif


#ifdef __SHADOWD__
        /* wait for shadowd to shutdown */
        shadowd_shutdown();
#endif

#ifdef __VFS__
        /* Shutdown the vfs: */
        dbg(DBG_PRINT, "weenix: vfs shutdown...\n");
        vput(curproc->p_cwd);
        if (vfs_shutdown())
		/*dbg(DBG_PRINT, "kmain283......\n");*/
                panic("vfs shutdown FAILED!!\n");

#endif

        /* Shutdown the pframe system */
#ifdef __S5FS__
        pframe_shutdown();
#endif

        dbg_print("\nweenix: halted cleanly!\n");
        GDB_CALL_HOOK(shutdown);
        hard_shutdown();
        return NULL;
}


#ifdef __DRIVERS__

int do_faber(kshell_t *kshell, int argc, char **argv){
	KASSERT(kshell != NULL);
        dbg(DBG_PRINT, "(GRADING1C)\n");
            /*
             * Shouldn't call a test function directly.
             * It's best to invoke it in a separate kernel process.  
             */
	
	proc_t * proc = NULL;
	kthread_t * kthread=NULL;
	int arg1=0;
	void *arg2=NULL;
	int status=0;

	proc = proc_create("faber");
	kthread = kthread_create(proc, faber_thread_test, arg1, arg2);
	sched_make_runnable(kthread);
	do_waitpid(-1, 0, &status);
        return 0;
}

int do_sunghan(kshell_t *kshell, int argc, char **argv){
	KASSERT(kshell != NULL);
        dbg(DBG_PRINT, "(GRADING1D 1)\n");
            /*
             * Shouldn't call a test function directly.
             * It's best to invoke it in a separate kernel process.  
             */
	proc_t * proc = NULL;
	kthread_t * kthread=NULL;
	int arg1=0;
	void *arg2=NULL;
	int status=0;
	
	proc = proc_create("sunghan");

	kthread = kthread_create(proc, sunghan_test, arg1, arg2);
	sched_make_runnable(kthread);
	do_waitpid(-1, 0, &status);
        return 0;
}

int do_deadlock(kshell_t *kshell, int argc, char **argv){
	KASSERT(kshell != NULL);
        dbg(DBG_PRINT, "(GRADING1D 2)\n");
            /*
             * Shouldn't call a test function directly.
             * It's best to invoke it in a separate kernel process.  
             */
	
	proc_t * proc = NULL;
	kthread_t * kthread=NULL;
	int arg1=0;
	void *arg2=NULL;
	int status=0;


	proc = proc_create("deadlock");
	kthread = kthread_create(proc, sunghan_deadlock_test, arg1, arg2);
	sched_make_runnable(kthread);
	do_waitpid(-1, 0, &status);
        return 0;
}

#ifdef __VFS__
int do_vfs(kshell_t *kshell, int argc, char **argv){
	KASSERT(kshell != NULL);
        dbg(DBG_PRINT, "(GRADING2B)\n");
            /*
             * Shouldn't call a test function directly.
             * It's best to invoke it in a separate kernel process.  
             */
	
	proc_t * proc = NULL;
	kthread_t * kthread=NULL;
	int arg1=1;/*ATTENTION!*/
	void *arg2=NULL;
	int status=0;

	proc = proc_create("vfs");
	kthread = kthread_create(proc, vfstest_main, arg1, arg2);
	sched_make_runnable(kthread);
	do_waitpid(-1, 0, &status);
        return 0;
}
#endif /* __VFS__ */

#endif /* __DRIVERS__ */


/**
 * This function, called by the idle process (within 'idleproc_run'), creates the
 * process commonly refered to as the "init" process, which should have PID 1.
 *
 * The init process should contain a thread which begins execution in
 * initproc_run().
 *
 * @return a pointer to a newly created thread which will execute
 * initproc_run when it begins executing
 */
static kthread_t *
initproc_create(void)
{	char init[9]="init_proc";
        proc_t * proc = NULL;
	kthread_t * kthread=NULL;
	int arg1=0;
	void *arg2=NULL;
	proc = proc_create(init);
	kthread=kthread_create(proc, initproc_run, arg1, arg2);
	
	KASSERT(NULL != proc);
	dbg(DBG_PRINT, "(GRADING1A 1.b)\n");
	KASSERT(PID_INIT == proc->p_pid);
	dbg(DBG_PRINT, "(GRADING1A 1.b)\n");
	KASSERT(kthread != NULL);
	dbg(DBG_PRINT, "(GRADING1A 1.b)\n");
	
        return kthread;
}

/**
 * The init thread's function changes depending on how far along your Weenix is
 * developed. Before VM/FI, you'll probably just want to have this run whatever
 * tests you've written (possibly in a new process). After VM/FI, you'll just
 * exec "/sbin/init".
 *
 * Both arguments are unused.
 *
 * @param arg1 the first argument (unused)
 * @param arg2 the second argument (unused)
 */
static pid_t wait_for_any() {
    int rv;
    pid_t pid;

    pid = do_waitpid(-1, 0, &rv);
    /*if ( pid != -ECHILD) dbg(DBG_TEST, "child (%d) exited: %d\n", pid, rv);*/
    return pid;
}

/*
 * Repeatedly call wait_for_any() until it returns -ECHILD
 */
static void wait_for_all() {
    while (wait_for_any() != -ECHILD)
        ;
}

static void *
initproc_run(int arg1, void *arg2)
{
#define mode 1

 #if mode
	char *a;
	char *argv[] = {NULL};
	
	char *argv_uname[] = { "/bin/uname","-a",NULL};
	char *argv_args[] = { "/usr/bin/args","-a",NULL};

    char *envp[] = { NULL };
    /*You can also use kernel_execve() to invoke other user space programs, such as "/usr/bin/hello", "/bin/uname", "/usr/bin/args", "/usr/bin/fork-and-wait", etc., directly from the kernel. It is highly recommended that you can have all of these programs run in user space perfectly before attempting /sbin/init. */
   	/*kernel_execve("usr/bin/hello",argv, envp);*/
    /*kernel_execve("/bin/uname",argv_uname,envp);*/

	/*kernel_execve("/usr/bin/args",argv_args,envp);*/
   	/*kernel_execve("/usr/bin/fork-and-wait",argv,envp);*/
    kernel_execve("/sbin/init", argv, envp);

#endif   
 #if ~mode			
				
        kshell_add_command("faber",do_faber, "invoke do_faber() to print a message...");
	kshell_add_command("sunghan", do_sunghan, "invoke do_sunghan() to print a message...");
kshell_add_command("deadlock", do_deadlock, "invoke do_deadlock() to print a message...");
	
	#ifdef __VFS__
	kshell_add_command("vfs", do_vfs, "Run vfstest_main().");
	kshell_add_command("thrtest", faber_fs_thread_test, "Run faber_fs_thread_test().");
        kshell_add_command("dirtest", faber_directory_test, "Run faber_directory_test().");
	#endif

        kshell_t *kshell = kshell_create(0);
        if (NULL == kshell) panic("init: Couldn't create kernel shell\n");
        while (kshell_execute_next(kshell)){
        	int status=0;
        	wait_for_all();
        }
				
        kshell_destroy(kshell);

 #endif /* __DRIVERS__ */
 	
        return NULL;
}


