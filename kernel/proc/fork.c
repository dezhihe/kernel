/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "errno.h"

#include "util/debug.h"
#include "util/string.h"

#include "proc/proc.h"
#include "proc/kthread.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/pframe.h"
#include "mm/mmobj.h"
#include "mm/pagetable.h"
#include "mm/tlb.h"

#include "fs/file.h"
#include "fs/vnode.h"

#include "vm/shadow.h"
#include "vm/vmmap.h"

#include "api/exec.h"

#include "main/interrupt.h"

/* Pushes the appropriate things onto the kernel stack of a newly forked thread
 * so that it can begin execution in userland_entry.
 * regs: registers the new thread should have on execution
 * kstack: location of the new thread's kernel stack
 * Returns the new stack pointer on success. */
static uint32_t
fork_setup_stack(const regs_t *regs, void *kstack)
{
        /* Pointer argument and dummy return address, and userland dummy return
         * address */
        uint32_t esp = ((uint32_t) kstack) + DEFAULT_STACK_SIZE - (sizeof(regs_t) + 12);
        *(void **)(esp + 4) = (void *)(esp + 8); /* Set the argument to point to location of struct on stack */
        memcpy((void *)(esp + 8), regs, sizeof(regs_t)); /* Copy over struct */
        return esp;
}


/*
 * The implementation of fork(2). Once this works,
 * you're practically home free. This is what the
 * entirety of Weenix has been leading up to.
 * Go forth and conquer.
 */
int
do_fork(struct regs *regs)
{
        /* NOT_YET_IMPLEMENTED("VM: do_fork")*/
	proc_t *child_proc = NULL;
	kthread_t *child_thread = NULL;
	
	KASSERT(regs != NULL);
	dbg(DBG_PRINT, "(GRADING3A 7.a)\n"); 
	KASSERT(curproc != NULL);
	dbg(DBG_PRINT, "(GRADING3A 7.a)\n"); 
	KASSERT(curproc->p_state == PROC_RUNNING);
	dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
	
	child_proc = proc_create("child_proc");
	KASSERT(child_proc != NULL);
	KASSERT(child_proc->p_state == PROC_RUNNING);
	dbg(DBG_PRINT, "(GRADING3A 7.a)\n"); 
	KASSERT(child_proc->p_pagedir != NULL);
	dbg(DBG_PRINT, "(GRADING3A 7.a)\n"); 
	
	
	child_proc->p_vmmap = vmmap_clone(curproc->p_vmmap);
	child_proc->p_vmmap->vmm_proc=child_proc;

	list_link_t *link_new = NULL;
	list_link_t *link_old = NULL;
	list_t *list_new = &child_proc->p_vmmap->vmm_list;
	list_t *list_old = &curproc->p_vmmap->vmm_list;
	vmarea_t *newvma = NULL;

	vmarea_t *oldvma = NULL;
	
	/*Handling shadow object and relating reference count*/
	for(link_old = list_old->l_next, link_new = list_new->l_next; link_old != list_old && link_new != list_new; link_old = link_old->l_next, link_new = link_new->l_next) {
		dbg(DBG_PRINT,"GRADING3B\n");
		oldvma = list_item(link_old, vmarea_t, vma_plink);
		newvma = list_item(link_new, vmarea_t, vma_plink); 
		if(newvma->vma_flags & MAP_PRIVATE){
			dbg(DBG_PRINT,"GRADING3B\n");
			/*Parent*/
			mmobj_t *shadow_for_old = NULL;
			shadow_for_old = shadow_create();
			KASSERT(shadow_for_old != NULL);
			shadow_for_old->mmo_shadowed = oldvma->vma_obj;
			shadow_for_old->mmo_un.mmo_bottom_obj = oldvma->vma_obj->mmo_un.mmo_bottom_obj;
			oldvma->vma_obj = shadow_for_old;
			/*Child*/
			mmobj_t *shadow_for_new = NULL;
			shadow_for_new = shadow_create();
			KASSERT(shadow_for_new != NULL);
			shadow_for_new->mmo_shadowed = shadow_for_old->mmo_shadowed;
			shadow_for_new->mmo_un.mmo_bottom_obj = oldvma->vma_obj->mmo_un.mmo_bottom_obj;
			newvma->vma_obj = shadow_for_new;
			/*After the whole process, the old object shadowed has incresed by one, also, we have to increse all the refcount down the shadow chain from the old shadow obj*/
			mmobj_t *shadow = shadow_for_new->mmo_shadowed;

			while(shadow != NULL) {
				dbg(DBG_PRINT,"GRADING3B\n");
				shadow->mmo_ops->ref(shadow);

				shadow = shadow->mmo_shadowed;

			}			


			
			list_insert_tail(mmobj_bottom_vmas(shadow_for_new), &newvma->vma_olink);
		}
		else{
			dbg(DBG_PRINT,"GRADING3B\n");
			newvma->vma_obj = oldvma->vma_obj;
			list_insert_tail(&newvma->vma_obj->mmo_un.mmo_vmas, &newvma->vma_olink);
			newvma->vma_obj->mmo_ops->ref(newvma->vma_obj);
		}

	}
	
	pt_unmap_range(curproc->p_pagedir, USER_MEM_LOW, USER_MEM_HIGH);
	
	tlb_flush_all();

	
	
	kthread_t *child_kthread = NULL;
	child_kthread = kthread_clone(curthr);

	KASSERT(child_kthread != NULL);
	KASSERT(child_kthread->kt_kstack != NULL);
	dbg(DBG_PRINT, "(GRADING3A 7.a)\n");


	regs->r_eax = 0;/*Return value is copied from eax register*/
	child_kthread->kt_ctx.c_pdptr= child_proc->p_pagedir;
	child_kthread->kt_ctx.c_eip = (uint32_t)userland_entry;
	child_kthread->kt_ctx.c_esp = fork_setup_stack(regs, child_kthread->kt_kstack);
	child_kthread->kt_ctx.c_kstack = (uint32_t)child_kthread->kt_kstack;
	child_kthread->kt_ctx.c_kstacksz = DEFAULT_STACK_SIZE;
	child_kthread->kt_proc = child_proc;
	list_insert_tail(&child_proc->p_threads,&child_kthread->kt_plink);

	int i=0;
	while(i<NFILES){
		dbg(DBG_PRINT,"GRADING3B\n");
		child_proc->p_files[i] = curproc->p_files[i];
		if(curproc->p_files[i] != NULL){
			dbg(DBG_PRINT,"GRADING3B\n");
			fref(curproc->p_files[i]);
		}
		i++;
	}					/*copy file table of parent_proc to child_proc, and increase refcount of files*/
	
	child_proc->p_brk = curproc->p_brk;
	child_proc->p_start_brk = curproc->p_start_brk; /*set other fields*/
	
	sched_make_runnable(child_kthread); /*make it runnable*/

	return child_proc->p_pid;
}

