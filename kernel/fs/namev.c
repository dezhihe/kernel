/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "globals.h"
#include "types.h"
#include "errno.h"

#include "util/string.h"
#include "util/printf.h"
#include "util/debug.h"

#include "fs/dirent.h"
#include "fs/fcntl.h"
#include "fs/stat.h"
#include "fs/vfs.h"
#include "fs/vnode.h"

/* This takes a base 'dir', a 'name', its 'len', and a result vnode.
 * Most of the work should be done by the vnode's implementation
 * specific lookup() function, but you may want to special case
 * "." and/or ".." here depnding on your implementation.
 *
 * If dir has no lookup(), return -ENOTDIR.
 *
 * Note: returns with the vnode refcount on *result incremented.
 */
int
lookup(vnode_t *dir, const char *name, size_t len, vnode_t **result)
{
         /*NOT_YET_IMPLEMENTED("VFS: lookup");*/
	dbg(DBG_PRINT,"(GRADING2A)\n");
        KASSERT(NULL != dir);
        dbg(DBG_PRINT,"(GRADING2A 2.a)\n");
        KASSERT(NULL != name);
        dbg(DBG_PRINT,"(GRADING2A 2.a)\n");
        KASSERT(NULL != result);
        dbg(DBG_PRINT,"(GRADING2A 2.a)\n");
	int ret;
	if((dir->vn_ops->lookup == NULL)||(!S_ISDIR(dir->vn_mode))){
		dbg(DBG_PRINT,"(GRADING2B)\n");
		return -ENOTDIR;


	}
	
	if(len>NAME_LEN){
		dbg(DBG_PRINT,"(GRADING2B)ENAMETOOLONG\n");
		return -ENAMETOOLONG;	
	}

	if(/*strcmp(name,".") == 0||*/len==0){
		dbg(DBG_PRINT,"(GRADING2B)\n");
		*result = dir;
		vref(dir);
		return 0;
	}
/*	
	if(strcmp(name,"..") == 0){
	
	*result = ?;
	vref(?);
	return 0;
	}
*/	
        return dir->vn_ops->lookup(dir,name,len,result);
}


/* When successful this function returns data in the following "out"-arguments:
 *  o res_vnode: the vnode of the parent directory of "name"
 *  o name: the `basename' (the element of the pathname)
 *  o namelen: the length of the basename
 *
 * For example: dir_namev("/s5fs/bin/ls", &namelen, &name, NULL,
 * &res_vnode) would put 2 in namelen, "ls" in name, and a pointer to the
 * vnode corresponding to "/s5fs/bin" in res_vnode.
 *
 * The "base" argument defines where we start resolving the path from:
 * A base value of NULL means to use the process's current working directory,
 * curproc->p_cwd.  If pathname[0] == '/', ignore base and start with
 * vfs_root_vn.  dir_namev() should call lookup() to take care of resolving each
 * piece of the pathname.
 *
 * Note: A successful call to this causes vnode refcount on *res_vnode to
 * be incremented.
 */
int
dir_namev(const char *pathname, size_t *namelen, const char **name,
          vnode_t *base, vnode_t **res_vnode)
{
	
        /*NOT_YET_IMPLEMENTED("VFS: dir_namev");*/
        KASSERT(NULL != pathname);
        dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
        KASSERT(NULL != namelen);
        dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
        KASSERT(NULL != name);
        dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
        KASSERT(NULL != res_vnode);
        dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
	vnode_t *dir= NULL;
	vnode_t *result =NULL;
	int numSlash=0;
	int i= 0;
	int j=0;
	int k=0;
	int offset_m = 0;
	int nouse=0;
	if(!strcmp(pathname,"/")){
		dbg(DBG_PRINT,"(GRADING2B)\n");
		nouse++;
	}
	int offset=0;
	int path_len=strlen(pathname);
	
	

	if(strlen(pathname)==0){
		dbg(DBG_PRINT,"(GRADING2B)EINVAL len=0\n");
		return -EINVAL;
	}
	
	char buf[NAME_LEN+2];/*1 for '', 1 for detecting long name*/
	memset(buf,'\0',sizeof(buf));
	
	char path_buf[MAXPATHLEN];
	memset(path_buf,'\0',sizeof(path_buf));
	
	strncpy(path_buf,pathname,strlen(pathname));
	for(j=0;j<path_len;j++){

		if(((path_buf[j] == '/') && (path_buf[j+1] != '/')))
		{
			dbg(DBG_PRINT, "(GRADING2B)\n");
			numSlash++;

		}
		if(((path_buf[j] == '/') && (path_buf[j+1] == '/')))
		{
			dbg(DBG_PRINT, "(GRADING2B)\n");
			offset++;

		}
	}

	for(j=0;j<path_len;j++){
		if(path_buf[path_len-1-j] == '/'){
			dbg(DBG_PRINT, "(GRADING2B)\n");
			offset_m++;
		}
		else{
			dbg(DBG_PRINT, "(GRADING2B)\n");
			break;
		}

	}
	if(offset_m >0){
		dbg(DBG_PRINT, "(GRADING2B)\n");
		offset=offset-(offset_m-1);
	}
	if((pathname[path_len-1] == '/') && strcmp(pathname,"/")){
		dbg(DBG_PRINT, "(GRADING2B)\n");
		numSlash--;
	}
	*name=pathname;
	int len= 0;
	int ret=0;
	char *tok=NULL;
	int n=0;	
	char*path_str=path_buf;
	
	if(path_buf[0] == '/'){
		dbg(DBG_PRINT, "(GRADING2A)\n");
		dir = vfs_root_vn; 
		vref(dir);
		numSlash--;
		(*name)++;
		path_str++;
	}
	
	else 
	{
		if(base == NULL){
			dbg(DBG_PRINT,"(GRADING2B)\n");
			dir =  curproc->p_cwd;
			vref(dir);
		
		}
		/*else{
			dbg(DBG_PRINT,"namev.c 199...........................\n");
			dir = base;
			vref(dir);
		
		}*/
	}



	if(numSlash ==0){
		dbg(DBG_PRINT, "(GRADING2A)\n");
		if(strcmp(pathname,"/")){
			dbg(DBG_PRINT, "(GRADING2A)\n");
			tok=strtok(path_str,"/");
			strncpy(buf,tok,strlen(tok));
			*res_vnode = dir;
			*name+=offset;
			*namelen=strlen(buf);
			memset(buf,'\0',sizeof(buf));
			return 0;
		}
			
	}
	
	else{
		dbg(DBG_PRINT, "(GRADING2A)\n");
		tok=strtok(path_str,"/");
		strncpy(buf,tok,strlen(tok));
	
		*namelen=strlen(buf);
		*name+=*namelen+1;
		KASSERT(NULL != dir);
		dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
		ret=lookup(dir,(const char*)buf,*namelen,&result);
		if(ret < 0){
			dbg(DBG_PRINT,"(GRADING2B)\n");
			if(dir != NULL) vput(dir);
			if(result != NULL) vput(result);
			return ret;
		}
	
	
		vput(dir);
	
		dir=result;
	
		for(i=0;i<numSlash-1;i++){
			
			memset(buf,'\0',sizeof(buf));
			tok=strtok(NULL,"/");
			strncpy(buf,tok,strlen(tok));
			dbg(DBG_PRINT,"(GRADING2B)\n");
			*namelen=strlen(buf);
			*name+=*namelen+1;
			KASSERT(NULL != dir);
		        dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
			ret=lookup(dir,(const char*)buf,*namelen,&result);
		
			if(ret <0){
				dbg(DBG_PRINT,"namev.c 255...........................\n");
				
				if(result != NULL) vput(result);
				return ret;
			}
			vput(dir);
			dir=result;	
		
		
		}
	}

	
	if((pathname[path_len-1] == '/' ) && strcmp(pathname,"/")){
		dbg(DBG_PRINT, "(GRADING2A)\n");
		tok=strtok(NULL,"/");
		strncpy(buf,tok,strlen(tok));
		*res_vnode = dir;
		*name+=offset;
		*namelen=strlen(buf);
		memset(buf,'\0',sizeof(buf));	
		return 0;	
		
	}	
	else{
		dbg(DBG_PRINT, "(GRADING2A)\n");
		*res_vnode = dir;
		*name+=offset;
		*namelen=strlen(*name);
		memset(buf,'\0',sizeof(buf));	
		return 0;	
	}
	
	

        return 0;
}

/* This returns in res_vnode the vnode requested by the other parameters.
 * It makes use of dir_namev and lookup to find the specified vnode (if it
 * exists).  flag is right out of the parameters to open(2); see
 * <weenix/fcntl.h>.  If the O_CREAT flag is specified, and the file does
 * not exist call create() in the parent directory vnode.
 *
 * Note: Increments vnode refcount on *res_vnode.
 */
int
open_namev(const char *pathname, int flag, vnode_t **res_vnode, vnode_t *base)
{	
	
        /*NOT_YET_IMPLEMENTED("VFS: open_namev");*/
	dbg(DBG_PRINT, "(GRADING2A)\n");
	size_t namelen=0;
	const char *name;
	vnode_t *result1 = NULL;
	vnode_t *result2 = NULL;
	int i;
	int j;
	int k;
	i=dir_namev(pathname,&namelen,&name,NULL,&result1);
	if(i<0){
		dbg(DBG_PRINT,"(GRADING2B)\n");
		if(result1 != NULL)
			vput(result1);
		return i;
	}
	else{
		dbg(DBG_PRINT, "(GRADING2B)\n");
		j= lookup(result1,name,namelen,&result2);
		if((j < 0) && (flag & O_CREAT) && (S_ISDIR(result1->vn_mode))){
			KASSERT(NULL != result1->vn_ops->create);
			dbg(DBG_PRINT,"(GRADING2A 2.c)\n");
	
			k = (result1->vn_ops->create)(result1, name,namelen, &result2);
			
			if(k<0){
				dbg(DBG_PRINT,"(GRADING2B)\n");
				if(result2 != NULL) vput(result2);
				vput(result1);
				return k;
				}
		}
		else if ((j< 0) && (~(flag & O_CREAT))){
			dbg(DBG_PRINT,"(GRADING2B)\n");
			if(result2 != NULL) vput(result2);
			vput(result1);
			return j;
			}
		*res_vnode = result2;
	}
	
	vput(result1);
        return 0;
}

#ifdef __GETCWD__
/* Finds the name of 'entry' in the directory 'dir'. The name is writen
 * to the given buffer. On success 0 is returned. If 'dir' does not
 * contain 'entry' then -ENOENT is returned. If the given buffer cannot
 * hold the result then it is filled with as many characters as possible
 * and a null terminator, -ERANGE is returned.
 *
 * Files can be uniquely identified within a file system by their
 * inode numbers. */
int
lookup_name(vnode_t *dir, vnode_t *entry, char *buf, size_t size)
{
        NOT_YET_IMPLEMENTED("GETCWD: lookup_name");
        return -ENOENT;
}


/* Used to find the absolute path of the directory 'dir'. Since
 * directories cannot have more than one link there is always
 * a unique solution. The path is writen to the given buffer.
 * On success 0 is returned. On error this function returns a
 * negative error code. See the man page for getcwd(3) for
 * possible errors. Even if an error code is returned the buffer
 * will be filled with a valid string which has some partial
 * information about the wanted path. */
ssize_t
lookup_dirpath(vnode_t *dir, char *buf, size_t osize)
{
        NOT_YET_IMPLEMENTED("GETCWD: lookup_dirpath");

        return -ENOENT;
}
#endif /* __GETCWD__ */
