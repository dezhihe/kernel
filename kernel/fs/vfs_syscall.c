/******************************************************************************/
/* Important Spring 2015 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 *  FILE: vfs_syscall.c
 *  AUTH: mcc | jal
 *  DESC:
 *  DATE: Wed Apr  8 02:46:19 1998
 *  $Id: vfs_syscall.c,v 1.10 2014/12/22 16:15:17 william Exp $
 */

#include "kernel.h"
#include "errno.h"
#include "globals.h"
#include "fs/vfs.h"
#include "fs/file.h"
#include "fs/vnode.h"
#include "fs/vfs_syscall.h"
#include "fs/open.h"
#include "fs/fcntl.h"
#include "fs/lseek.h"
#include "mm/kmalloc.h"
#include "util/string.h"
#include "util/printf.h"
#include "fs/stat.h"
#include "util/debug.h"

/* Comment by Xunzhi.
 * open_namev should implement this!
 * checking
 	[for path] ENOENT ENAMETOOLONG (namelen>NAME_LEN)
 	[for path of dir('component of path') not vn!] ENOTDIR 
 	[not including] EISDIR 
 * Return 0 or errno
 */
 

/* To read a file:
 *      o fget(fd)
 *      o call its virtual read fs_op
 *      o update f_pos
 *      o fput() it
 *      o return the number of bytes read, or an error
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not a valid file descriptor or is not open for reading.
 *      o EISDIR
 *        fd refers to a directory.
 *
 * In all cases, be sure you do not leak file refcounts by returning before
 * you fput() a file that you fget()'ed.
 */
int
do_read(int fd, void *buf, size_t nbytes)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_read");*/
	dbg(DBG_PRINT, "(GRADING2A)\n");

	if(fd < 0 || fd >= NFILES) {
		dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        
	int len = -1;
        file_t *f=fget(fd);
        if(f==NULL){
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }      
        if (!((f->f_mode) & FMODE_READ)){
		fput(f);
		dbg(DBG_PRINT,"(GRADING2B)EBADF no reading\n");
		return -EBADF;		
	}
	if (S_ISDIR(f->f_vnode->vn_mode)){
		fput(f);
		dbg(DBG_PRINT,"(GRADING2B)EISDIR\n");
		return -EISDIR;	
	}				
	KASSERT(NULL != f->f_vnode->vn_ops->read);
        dbg(DBG_PRINT,"(GRADING2A)can read\n");
	len = f->f_vnode->vn_ops->read(f->f_vnode, f->f_pos, buf, nbytes);
				/*is the following necessary?
				
				if(len < 0){
					fput(f);
					dbg(DBG_PRINT,"len<0?\n");
					return len;		
				}
				*/
	f->f_pos += len;
	fput(f);

        return len;
}

/* Very similar to do_read.  Check f_mode to be sure the file is writable.  If
 * f_mode & FMODE_APPEND, do_lseek() to the end of the file, call the write
 * fs_op, and fput the file.  As always, be mindful of refcount leaks.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not a valid file descriptor or is not open for writing.
 */
int
do_write(int fd, const void *buf, size_t nbytes)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_write");*/
        dbg(DBG_PRINT, "(GRADING2A)\n");
        if(fd < 0 || fd >= NFILES) {
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        
	int len = -1;
	int i;
	file_t *f=fget(fd);
	if(f==NULL){
		dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
	      	return -EBADF;
	}      
	if(!((f->f_mode)&FMODE_WRITE)){					
		dbg(DBG_PRINT,"(GRADING2B)EBADF no writing\n");
		fput(f);
		return -EBADF;
	}
	if (S_ISDIR(f->f_vnode->vn_mode))
	{
		dbg(DBG_PRINT,"EISDIR\n");
		fput(f);
		return -EISDIR;			
	}
	int err = 0;
	if((f->f_mode) & FMODE_APPEND){
		i = f->f_vnode->vn_len;
		err = do_lseek(fd,i,SEEK_SET);
		if(err < 0) {
			fput(f);
			return err;
		}
		dbg(DBG_PRINT,"(GRADING2B)FMODE_APPEND\n");
	}	
					
	KASSERT(NULL != f->f_vnode->vn_ops->write);
      	dbg(DBG_PRINT,"(GRADING2A)can write\n");
	len = f->f_vnode->vn_ops->write(f->f_vnode, f->f_pos, buf, nbytes);			
	if(len < 0){						
		fput(f);
		dbg(DBG_PRINT,"write return value invalid in vfs_syscall.c\n");
		return -EBADF;	
	}
					
	f->f_pos += len;
	fput(f);
	KASSERT((S_ISCHR(f->f_vnode->vn_mode)) ||(S_ISBLK(f->f_vnode->vn_mode)) ||((S_ISREG(f->f_vnode->vn_mode)) && (f->f_pos <= f->f_vnode->vn_len)));				
	dbg(DBG_PRINT,"(GRADING2A 3.a)\n");
									
	return len;

}

/*
 * Zero curproc->p_files[fd], and fput() the file. Return 0 on success
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd isn't a valid open file descriptor.
 */
int
do_close(int fd)
{
    /*NOT_YET_IMPLEMENTED("VFS: do_close");*/
	if(fd < 0 || fd >= NFILES) {
		dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
		return -EBADF;
	}	
	file_t *f=fget(fd);
    if(f == NULL){
    	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
    	return -EBADF;
    }
	curproc->p_files[fd] = NULL;
	fput(f);
	fput(f);
	dbg(DBG_PRINT,"(GRADING2B)Done\n");
	return 0;
}

/* To dup a file:
 *      o fget(fd) to up fd's refcount
 *      o get_empty_fd()
 *      o point the new fd to the same file_t* as the given fd
 *      o return the new file descriptor
 *
 * Don't fput() the fd unless something goes wrong.  Since we are creating
 * another reference to the file_t*, we want to up the refcount.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd isn't an open file descriptor.
 *      o EMFILE
 *        The process already has the maximum number of file descriptors open
 *        and tried to open a new one.
 */
int
do_dup(int fd)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_dup");*/
	dbg(DBG_PRINT,"(GRADING2B)\n");
        if(fd < 0 || fd >= NFILES) {
       		dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        int newfd=0;
        file_t *f=fget(fd);
        if(f==NULL){
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }        
        newfd=get_empty_fd(curproc);/*can be EMFILE*/
        
/*if(newfd<0){
	dbg(DBG_PRINT,"syscall.c 231...................EMFILE or get empty fd err\n");
	fput(f);
	return newfd;
}*/
        
        curproc->p_files[newfd]=f;
        dbg(DBG_PRINT,"(GRADING2B)point the new fd to the same file_t* as the given fd\n");
        return newfd;
}

/* Same as do_dup, but insted of using get_empty_fd() to get the new fd,
 * they give it to us in 'nfd'.  If nfd is in use (and not the same as ofd)
 * do_close() it first.  Then return the new file descriptor.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        ofd isn't an open file descriptor, or nfd is out of the allowed
 *        range for file descriptors.
 */
int
do_dup2(int ofd, int nfd)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_dup2");*/
	dbg(DBG_PRINT,"(GRADING2B)\n");
        if(nfd < 0 || nfd >= NFILES || ofd < 0 || ofd >= NFILES){
        	dbg(DBG_PRINT,"(GRADING2B)nfd EBADF\n");
        	return -EBADF;
        }
        
        file_t *of=fget(ofd);
        if(of == NULL){
        	dbg(DBG_PRINT,"(GRADING2B)ofd EBADF\n");
        	return -EBADF;
        }
        if(ofd == nfd) {
        	dbg(DBG_PRINT,"(GRADING2B)ofd==nfd\n");
        	fput(of);
        	return nfd;
        }
        
        /*In case of ofd != nfd*/
        file_t *nf = fget(nfd);
        if(nf!=NULL){
        	dbg(DBG_PRINT,"(GRADING2B)nfd is in use\n");
        	fput(nf);
        	do_close(nfd);
        }
                       
        curproc->p_files[nfd]=of;
        dbg(DBG_PRINT,"(GRADING2B)ok\n");
        return nfd;
}

/*
 * This routine creates a special file of the type specified by 'mode' at
 * the location specified by 'path'. 'mode' should be one of S_IFCHR or
 * S_IFBLK (you might note that mknod(2) normally allows one to create
 * regular files as well-- for simplicity this is not the case in Weenix).
 * 'devid', as you might expect, is the device identifier of the device
 * that the new special file should represent.
 *
 * You might use a combination of dir_namev, lookup, and the fs-specific
 * mknod (that is, the containing directory's 'mknod' vnode operation).
 * Return the result of the fs-specific mknod, or an error.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EINVAL
 *        mode requested creation of something other than a device special
 *        file.
 *      o EEXIST
 *        path already exists.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_mknod(const char *path, int mode, unsigned devid)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_mknod");*/
        /*int (*mknod)(struct vnode *dir, const char *name, size_t name_len,
                     int mode, devid_t devid);*/
	dbg(DBG_PRINT, "(GRADING2A)\n");

/*if(mode!=S_IFCHR && mode!=S_IFBLK){
	dbg(DBG_PRINT,"syscall.c 317.........................EINVAL\n");
	return -EINVAL;
}*/
    vnode_t *vn=NULL;
    size_t namelen=0;
    const char *name=NULL;
    vnode_t *dir=NULL;
    int ret=0;
    ret=dir_namev(path, &namelen, &name, NULL, &dir);
/*if(ret!=0){
	dbg(DBG_PRINT,"syscall.c 327........................dir_namev err %d\n",ret);
	return ret;
}*/
	ret=lookup(dir,name,namelen,&vn);
/*if(ret==0){
	dbg(DBG_PRINT,"syscall.c 332..........................EEXIST\n");
	vput(vn);
	vput(dir);
	return -EEXIST;
}*/
	
    KASSERT(NULL != dir->vn_ops->mknod);
    dbg(DBG_PRINT,"(GRADING2A 3.b)\n");
    ret=dir->vn_ops->mknod(dir,name,namelen,mode,devid);
   	vput(dir);/*dir_namev*/
    return ret;
}

/* Use dir_namev() to find the vnode of the dir we want to make the new
 * directory in.  Then use lookup() to make sure it doesn't already exist.
 * Finally call the dir's mkdir vn_ops. Return what it returns.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EEXIST
 *        path already exists.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_mkdir(const char *path)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_mkdir");*/
        /*int (*mkdir)(struct vnode *dir,  const char *name, size_t name_len);*/
	dbg(DBG_PRINT, "(GRADING2A)\n");
	
        vnode_t *vn=NULL;
        size_t namelen=0;
        const char *name=NULL;
        vnode_t *dir=NULL;
        int ret=0;
        ret=dir_namev(path, &namelen, &name, NULL, &dir);
        if(ret!=0){
        	dbg(DBG_PRINT,"(GRADING2B)dir_namev err%d\n",ret);
        	if(dir != NULL) vput(dir);
        	return ret;
        }       
   	if(!S_ISDIR(dir->vn_mode)){
        	dbg(DBG_PRINT,"(GRADING2B)ENOTDIR\n");
        	vput(dir);/*dir_namev*/
        	return -ENOTDIR;
        }
        ret=lookup(dir,name,namelen,&vn);
	if(ret==0){
		dbg(DBG_PRINT,"(GRADING2B)EEXIST\n");
		vput(vn);/*lookup*/
		vput(dir);/*dir_namev*/
        	return -EEXIST;
	}
	if(ret==-ENAMETOOLONG){
		dbg(DBG_PRINT,"(GRADING2B)ENAMETOOLONG\n");
		if(vn != NULL) vput(vn);
		if(dir != NULL) vput(dir);/*dir_namev*/
        	return ret;
	}
        KASSERT(NULL != dir->vn_ops->mkdir);
        dbg(DBG_PRINT,"(GRADING2A 3.c)\n");
        ret=dir->vn_ops->mkdir(dir,name,namelen);
		vput(dir);
       	if(vn != NULL) vput(vn);
        return ret;
}

/* Use dir_namev() to find the vnode of the directory containing the dir to be
 * removed. Then call the containing dir's rmdir v_op.  The rmdir v_op will
 * return an error if the dir to be removed does not exist or is not empty, so
 * you don't need to worry about that here. Return the value of the v_op,
 * or an error.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EINVAL
 *        path has "." as its final component.
 *      o ENOTEMPTY
 *        path has ".." as its final component.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_rmdir(const char *path)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_rmdir");*/
        /*int (*rmdir)(struct vnode *dir,  const char *name, size_t name_len);*/
	dbg(DBG_PRINT,"(GRADING2B)\n");

        vnode_t *vn=NULL;
        size_t namelen=0;
        const char *name=NULL;
        vnode_t *dir=NULL;
        int ret=0;        
        ret=dir_namev(path, &namelen, &name, NULL, &dir);                
        if(ret!=0){
        	dbg(DBG_PRINT,"(GRADING2B)dir_namev err\n");
        	if(dir != NULL) vput(dir);
        	return ret;
        }   
        if(namelen==1&&name[0]=='.'){
			dbg(DBG_PRINT,"(GRADING2B)EINVAL\n");
			vput(dir);/*dir_namev*/
			return -EINVAL;				
		}
		if(namelen==2&&name[0]=='.'&&name[1]=='.'){
			dbg(DBG_PRINT,"(GRADING2B)ENOTEMPTY\n");
			vput(dir);/*dir_namev*/
			return -ENOTEMPTY;				
		}            
		    ret=lookup(dir,name,namelen,&vn);
		if(ret!=0){
			dbg(DBG_PRINT,"(GRADING2B)lookup err\n");
			if(vn != NULL) vput(vn);
			vput(dir);/*dir_namev*/
		    	return ret;
		}
	
		vput(vn);/*lookup*/
	
        if(!S_ISDIR(vn->vn_mode)){
        	dbg(DBG_PRINT,"(GRADING2B)ENOTDIR\n");	
        	vput(dir);/*dir_namev*/
		return -ENOTDIR;
	}				
        KASSERT(NULL != dir->vn_ops->rmdir);
        dbg(DBG_PRINT,"(GRADING2A 3.d)\n");
        ret=dir->vn_ops->rmdir(dir,name,namelen);
        vput(dir);/*dir_namev*/
        return ret;
}

/*
 * Same as do_rmdir, but for files.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EISDIR
 *        path refers to a directory.
 *      o ENOENT
 *        A component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_unlink(const char *path)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_unlink");*/
        /*int (*unlink)(struct vnode *dir, const char *name, size_t name_len);*/
	dbg(DBG_PRINT,"(GRADING2B)\n");
	
        vnode_t *vn=NULL;
        size_t namelen=0;
        const char *name=NULL;
        vnode_t *dir=NULL;
        int ret=0;
        ret=dir_namev(path, &namelen, &name, NULL, &dir);
    /*if(ret!=0){
    	dbg(DBG_PRINT,"syscall.c 494...........................dir_namev err\n");
    	return ret;
    } */    
        ret=lookup(dir,name,namelen,&vn);
		if(ret!=0){
			dbg(DBG_PRINT,"(GRADING2B)lookup err\n");
			vput(dir);/*dir_namev*/
			if(vn != NULL) vput(vn);
		   	return ret;
		}
		vput(vn);/*lookup*/
		if(S_ISDIR(vn->vn_mode)){
			dbg(DBG_PRINT,"(GRADING2B)EISDIR\n");       	
			vput(dir);/*dir_namev*/
			return -EISDIR;
		}
        KASSERT(NULL != dir->vn_ops->unlink);
        dbg(DBG_PRINT,"(GRADING2A 3.e)\n");
        ret=dir->vn_ops->unlink(dir,name,namelen);
        vput(dir);/*dir_namev*/
        return ret;
}

/* To link:
 *      o open_namev(from)
 *      o dir_namev(to)
 *      o call the destination dir's (to) link vn_ops.
 *      o return the result of link, or an error
 *
 * Remember to vput the vnodes returned from open_namev and dir_namev.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EEXIST
 *        to already exists.
 *      o ENOENT
 *        A directory component in from or to does not exist.
 *      o ENOTDIR
 *        A component used as a directory in from or to is not, in fact, a
 *        directory.
 *      o ENAMETOOLONG
 *        A component of from or to was too long.
 *      o EISDIR
 *        from is a directory.
 */
int
do_link(const char *from, const char *to)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_link");*/
      #ifdef __VM__
        /*int (*link)(struct vnode *oldvnode, struct vnode *dir, 
        				const char *name, size_t name_len);*/
        vnode_t *vn=NULL;		    
		    int ret=open_namev(from, 0, &vn, NULL);
		    KASSERT(!ret);
/*		    if(ret!=0){
        	dbg(DBG_PRINT,"open from err\n");
        	return ret;
        }*/         
        if(S_ISDIR(vn->vn_mode)){
        	dbg(DBG_PRINT,"(GRADING3D)from should not be dir\n");
					vput(vn);
					return -EISDIR;
				}
				size_t namelen=0;
		    const char *name=NULL;
		    vnode_t *dir=NULL;				
        ret=dir_namev(to, &namelen, &name, NULL, &dir);
        KASSERT(!ret);
/*        if(ret!=0){
        	dbg(DBG_PRINT,"dir of to err\n");
        	return ret;
        } */
        ret=lookup(dir,name,namelen,&vn);
		if(ret==0){
			dbg(DBG_PRINT,"(GRADING3D)to exists\n");					
			vput(vn);/*lookup*/        	
			vput(dir);/*dir_namev*/
			vput(vn);/*open_namev*/
			return -EEXIST;
		}
		if(ret!=-ENOENT){
			dbg(DBG_PRINT,"(GRADING3D)lookup other err\n");
			vput(dir);/*dir_namev*/
			vput(vn);/*open_namev*/
			return ret;
		}
		KASSERT(NULL != dir->vn_ops->link);
        dbg(DBG_PRINT,"(GRADING3D)can call link\n");
        ret=dir->vn_ops->link(vn,dir,name,namelen);        
        vput(dir);/*dir_namev*/
        vput(vn);/*open_namev*/
        return ret;
      #endif
      /*return -1;*/
}

/*      o link newname to oldname
 *      o unlink oldname
 *      o return the value of unlink, or an error
 *
 * Note that this does not provide the same behavior as the
 * Linux system call (if unlink fails then two links to the
 * file could exist).
 */
int
do_rename(const char *oldname, const char *newname)
{
        NOT_YET_IMPLEMENTED("VFS: do_rename");
        /*int ret=do_link(oldname,newname);
        if(ret!=0){
        	dbg(DBG_PRINT,"link err\n");
        	return ret;
        }
        dbg(DBG_PRINT,"can unlink; link new to old=from old to new?\n");
        return do_unlink(oldname);*/
        return -1;
}

/* Make the named directory the current process's cwd (current working
 * directory).  Don't forget to down the refcount to the old cwd (vput()) and
 * up the refcount to the new cwd (open_namev() or vget()). Return 0 on
 * success.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o ENOENT
 *        path does not exist.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 *      o ENOTDIR
 *        A component of path is not a directory.
 */
int
do_chdir(const char *path)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_chdir");*/
	dbg(DBG_PRINT,"(GRADING2B)\n");

        vnode_t *vn=NULL;
        int ret=open_namev(path, 0, &vn, NULL);
        if(ret!=0){
        	dbg(DBG_PRINT,"(GRADING2B)open path err\n");
        	if(vn != NULL) vput(vn);
        	return ret;
        }
        if(!S_ISDIR(vn->vn_mode)){
        	dbg(DBG_PRINT,"(GRADING2B)ENOTDIR\n");
        	vput(vn);
        	return -ENOTDIR;
        }
        vput(curproc->p_cwd);
      	curproc->p_cwd=vn;
    
      	dbg(DBG_PRINT,"(GRADING2B)success\n");
      	return 0;
}

/* Call the readdir fs_op on the given fd, filling in the given dirent_t*.
 * If the readdir fs_op is successful, it will return a positive value which
 * is the number of bytes copied to the dirent_t.  You need to increment the
 * file_t's f_pos by this amount.  As always, be aware of refcounts, check
 * the return value of the fget and the virtual function, and be sure the
 * virtual function exists (is not null) before calling it.
 *
 * Return either 0 or sizeof(dirent_t), or -errno.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        Invalid file descriptor fd.
 *      o ENOTDIR
 *        File descriptor does not refer to a directory.
 */
int
do_getdent(int fd, struct dirent *dirp)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_getdent");*/
        /*int (*readdir)(struct vnode *dir, off_t offset, struct dirent *d);*/
        /*be aware of refcounts?ENOTDIR?*/
	dbg(DBG_PRINT,"(GRADING2B)\n");	
	
        if(fd < 0 || fd >= NFILES) {
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        int ret=0;
        /*off_t offset=0;*/
        file_t *f=fget(fd);
        if(f==NULL){
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        if(!S_ISDIR(f->f_vnode->vn_mode)){
        	dbg(DBG_PRINT,"(GRADING2B)ENOTDIR\n");
		fput(f);/*fget*/
		return -ENOTDIR;
	}
		/*Make sure the readdir exists*/
        KASSERT(NULL != f->f_vnode->vn_ops->readdir);
	dbg(DBG_PRINT,"(GRADING2B)\n");	

        ret=f->f_vnode->vn_ops->readdir(f->f_vnode,f->f_pos,dirp);
        if(ret > 0) {
		
        	f->f_pos+=ret;
        	fput(f);
        	dbg(DBG_PRINT,"(GRADING2B)readdir result %d\n",ret);
        	return sizeof(*dirp);/*sizeof(dirent_t)*/	
        }
        fput(f);/*dec ref as inc in fget*/
        dbg(DBG_PRINT,"(GRADING2B)readdir result %d\n",ret);   
        return 0;
        /*Return either 0 or sizeof(dirent_t), or -errno.*/
}

/*
 * Modify f_pos according to offset and whence.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not an open file descriptor.
 *      o EINVAL
 *        whence is not one of SEEK_SET, SEEK_CUR, SEEK_END; or the resulting
 *        file offset would be negative.
 */
int
do_lseek(int fd, int offset, int whence)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_lseek");*/
	dbg(DBG_PRINT,"(GRADING2B)\n");	
        if(fd < 0 || fd >= NFILES) {
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        off_t pos=0;
        file_t *f=fget(fd);
        if(f==NULL){
        	dbg(DBG_PRINT,"(GRADING2B)EBADF\n");
        	return -EBADF;
        }
        switch(whence){
		      case SEEK_SET: 
		      	pos=(off_t)offset;
		      	dbg(DBG_PRINT,"(GRADING2B)SEEK_SET\n");
		      	break;
		      case SEEK_CUR: 
		      	pos=f->f_pos+(off_t)offset;
		      	dbg(DBG_PRINT,"(GRADING2B)SEEK_CUR\n");
		      	break;
		      case SEEK_END: 
		      	pos=f->f_vnode->vn_len+(off_t)offset;
		      	dbg(DBG_PRINT,"(GRADING2B)SEEK_END\n");
		      	break;
		      default: 
		      	dbg(DBG_PRINT,"(GRADING2B)EBADF wrong whence\n");
		      	fput(f);/*fget*/
		      	return -EINVAL;
        }
        
        if(pos<0){
        	dbg(DBG_PRINT,"(GRADING2B)EBADF negative final pos\n"); 
        	fput(f);/*fget*/
        	return -EINVAL;
        }
        
        f->f_pos=pos;
        fput(f);/*fget*/
        return pos;
}

/*
 * Find the vnode associated with the path, and call the stat() vnode operation.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o ENOENT
 *        A component of path does not exist.
 *      o ENOTDIR
 *        A component of the path prefix of path is not a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */        
int
do_stat(const char *path, struct stat *buf)
{
        /*NOT_YET_IMPLEMENTED("VFS: do_stat");*/
        /*int (*stat)(struct vnode *vnode, struct stat *buf);*/
	dbg(DBG_PRINT,"(GRADING2B)\n");	

        vnode_t *vn=NULL;
        int ret=open_namev(path, 0, &vn, NULL);   
        if(ret!=0){
        	dbg(DBG_PRINT,"(GRADING2B)open namev err%d\n",ret);
        	if(vn != NULL) vput(vn);
        	return ret;
        }     
        KASSERT(vn->vn_ops->stat);
        dbg(DBG_PRINT,"(GRADING2A 3.f)\n");
        ret=vn->vn_ops->stat(vn,buf);
        vput(vn);/*open_namev*/
        return ret;
}

#ifdef __MOUNTING__
/*
 * Implementing this function is not required and strongly discouraged unless
 * you are absolutely sure your Weenix is perfect.
 *
 * This is the syscall entry point into vfs for mounting. You will need to
 * create the fs_t struct and populate its fs_dev and fs_type fields before
 * calling vfs's mountfunc(). mountfunc() will use the fields you populated
 * in order to determine which underlying filesystem's mount function should
 * be run, then it will finish setting up the fs_t struct. At this point you
 * have a fully functioning file system, however it is not mounted on the
 * virtual file system, you will need to call vfs_mount to do this.
 *
 * There are lots of things which can go wrong here. Make sure you have good
 * error handling. Remember the fs_dev and fs_type buffers have limited size
 * so you should not write arbitrary length strings to them.
 */
int
do_mount(const char *source, const char *target, const char *type)
{
        NOT_YET_IMPLEMENTED("MOUNTING: do_mount");
        return -EINVAL;
}

/*
 * Implementing this function is not required and strongly discouraged unless
 * you are absolutley sure your Weenix is perfect.
 *
 * This function delegates all of the real work to vfs_umount. You should not worry
 * about freeing the fs_t struct here, that is done in vfs_umount. All this function
 * does is figure out which file system to pass to vfs_umount and do good error
 * checking.
 */
int
do_umount(const char *target)
{
        NOT_YET_IMPLEMENTED("MOUNTING: do_umount");
        return -EINVAL;
}
#endif

