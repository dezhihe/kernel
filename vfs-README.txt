Documentation for Kernel Assignment 2
=====================================

+-------------+
| BUILD & RUN |
+-------------+

Same as what grading guidline indicates.

make clean
make
./weenix -n
Please use "help" command in kshell to see test commands.

+-----------------+
| SKIP (Optional) |
+-----------------+

(Left blank.)

+---------+
| GRADING |
+---------+

(A.1) In fs/vnode.c:
    (a) In special_file_read(): 6 out of 6 pts
    (b) In special_file_write(): 6 out of 6 pts

(A.2) In fs/namev.c:
    (a) In lookup(): 6 out of 6 pts
    (b) In dir_namev(): 10 out of 10 pts
    (c) In open_namev(): 2 out of 2 pts

(A.3) In fs/vfs_syscall.c:
    (a) In do_write(): 6 out of 6 pts
    (b) In do_mknod(): 2 out of 2 pts
    (c) In do_mkdir(): 2 out of 2 pts
    (d) In do_rmdir(): 2 out of 2 pts
    Need to run vfstest
    (e) In do_unlink(): 2 out of 2 pts
    Need to run vfstest
    (f) In do_stat(): 2 out of 2 pts
    Need to run vfstest

(B) vfstest: 39 out of 39 pts
    Comments: all passed

(C.1) faber_fs_thread_test (3 out of 3 pts)
(C.2) faber_directory_test (2 out of 2 pts)

(D) Self-checks: (10 out of 10 pts)
    Comments: we are not using additional code path.

Missing required section(s) in README file (vfs-README.txt): (-0pts) 
Submitted binary file : (-0pts) 
Submitted extra (unmodified) file : (-0pts) 
Wrong file location in submission : (-0pts) 
Use dbg_print(...) instead of dbg(DBG_PRINT, ...) : (-0pts) 
Not properly identify which dbg() printout is for which item in the grading guidelines : (-0pts) 
Cannot compile : (-0pts) 
Compiler warnings : (-0pts) 
"make clean" : (-0pts) 
Useless KASSERT : (-0pts) 
Insufficient/Confusing dbg : (-0pts) 
Kernel panic : (-0pts) 
Cannot halt kernel cleanly : (-0pts) 

+------+
| BUGS |
+------+

(Left blank.)

+---------------------------+
| CONTRIBUTION FROM MEMBERS |
+---------------------------+

Equal-share contribution.

+------------------+
| OTHER (Optional) |
+------------------+

Special DBG setting in Config.mk for certain tests: No
Comments on deviation from spec: No deviation from spec
General comments on design decisions: (Left blank.)


