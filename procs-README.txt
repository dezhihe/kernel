Documentation for Kernel Assignment 1
=====================================

+-------------+
| BUILD & RUN |
+-------------+

Same as what grading guidline indicates.

make clean
make
./weenix -n

+-----------------+
| SKIP (Optional) |
+-----------------+

(Left blank.)

+---------+
| GRADING |
+---------+
Expected printed messages are given. If not specified, messages are printed when simply starting the kernel.

(A.1) In main/kmain.c:
    (a) In bootstrap(): 3 out of 3 pts
    	(1 pts)190 bootstrap(): (GRADING1A 1.a)
    	(1 pts)192 bootstrap(): (GRADING1A 1.a)
    	(1 pts)194 bootstrap(): (GRADING1A 1.a)
    (b) In initproc_create(): 3 out of 3 pts
    	(1 pts)371 initproc_create(): (GRADING1A 1.b)
    	(1 pts)373 initproc_create(): (GRADING1A 1.b)
    	(1 pts)375 initproc_create(): (GRADING1A 1.b)

(A.2) In proc/proc.c:
    (a) In proc_create(): 4 out of 4 pts
    	(2 pts)143 proc_create(): (GRADING1A 2.a)    	
    (b) In proc_cleanup(): 5 out of 5 pts   
    In faber_thread_test{ 	
    	(1 pts)200 proc_cleanup(): (GRADING1A 2.b)
    	(1 pts)202 proc_cleanup(): (GRADING1A 2.b)
    	(2 pts)204 proc_cleanup(): (GRADING1A 2.b)
    	(1 pts)236 proc_cleanup(): (GRADING1A 2.b)
    }
    (c) In do_waitpid(): 8 out of 8 pts
    	(2 pts)367 do_waitpid(): (GRADING1A 2.c)
    In faber_thread_test{
    	(2 pts)378 do_waitpid(): (GRADING1A 2.c)
    	(2 pts)386 do_waitpid(): (GRADING1A 2.c)
    	(2 pts)394 do_waitpid(): (GRADING1A 2.c)
    }

(A.3) In prochread.c:
    (a) In kthread_create(): 2 out of 2 pts
    	(2 pts) 102 kthread_create(): (GRADING1A 3.a)
    (b) In kthread_cancel(): 1 out of 1 pt
    	(1 pts)137 kthread_cancel(): (GRADING1A 3.b)
    (c) In kthread_exit(): 3 out of 3 pts
    In faber_thread_test{
    	(1 pts)172 kthread_exit(): (GRADING1A 3.b)
    	(1 pts)174 kthread_exit(): (GRADING1A 3.b)
    	(1 pts)176 kthread_exit(): (GRADING1A 3.b)
    }

(A.4) In proc/sched.c:
    (a) In sched_wakeup_on(): 1 out of 1 pt
    Invoke faber_thread_test{
    	(1 pts)190 sched_wakeup_on(): (GRADING1A 4.a)
    }
    (b) In sched_make_runnable(): 1 out of 1 pt
    Invoke faber_thread_test{
    	(1 pts)318 sched_make_runnable(): (GRADING1A 4.b)
    }

(A.5) In proc/kmutex.c:
    (a) In kmutex_lock(): 1 out of 1 pt
    	(1 pts)45 kmutex_lock(): (GRADING1A 5.a)
    (b) In kmutex_lock_cancellable(): 1 out of 1 pt
    In faber_thread_test{
    	(1 pts)65 kmutex_lock_cancellable(): (GRADING1A 5.b)
    }
    (c) In kmutex_unlock(): 2 out of 2 pts
    In faber_thread_test{
    	(1 pts)98 kmutex_unlock(): (GRADING1A 5.c)
    	(1 pts)110 kmutex_unlock(): (GRADING1A 5.c)
    }

(B) Kshell : 20 out of 20 pts
    "help", "echo" and "exit" commands work properly
    "faber" or "f" command for faber_thread_test
    "sunghan" or "s" command for sunghan_test
    "deadlock" or "d" command for sunghan_deadlock_test

(C.1) waitpid any test, etc. (4 out of 4 pts)
(C.2) Context switch test (1 out of 1 pt)
(C.3) wake me test, etc. (2 out of 2 pts)
(C.4) wake me uncancellable test, etc. (2 out of 2 pts)
(C.5) cancel me test, etc. (4 out of 4 pts)
(C.6) reparenting test, etc. (2 out of 2 pts)
(C.7) show race test, etc. (3 out of 3 pts)
(C.8) kill child procs test (2 out of 2 pts)
(C.9) proc kill all test (2 out of 2 pts)

(D.1) sunghan_test(): producer/consumer test (9 out of 9 pts)
(D.2) sunghan_deadlock_test(): deadlock test (4 out of 4 pts)

(E) Additional self-checks: (10 out of 10 pts)
    No additional self-checks. All code paths tested.

Missing required section(s) in README file (procs-README.txt): (-0pts)
Submitted binary file : (-0pts)
Submitted extra (unmodified) file : (-0pts)
Wrong file location in submission : (-0pts)
Use dbg_print(...) instead of dbg(DBG_PRINT, ...) : (-0pts)
Not properly identify which dbg() printout is for which item in the grading guidelines : (-0pts)
Cannot compile : (-0pts)
Compiler warnings : (-0pts)
"make clean" : (-0pts) 
Useless KASSERT : (-0pts)
Insufficient/Confusing dbg : (-0pts)
Kernel panic : (-0pts)
Cannot halt kernel cleanly : (-0pts)

+------+
| BUGS |
+------+

(Left blank.)

+---------------------------+
| CONTRIBUTION FROM MEMBERS |
+---------------------------+

Equal-share contribution.

+------------------+
| OTHER (Optional) |
+------------------+

Special DBG setting in Config.mk for certain tests: No
Comments on deviation from spec: No deviation from spec
General comments on design decisions: (Left blank.)


